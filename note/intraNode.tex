\documentclass{article}
\usepackage[a4paper]{geometry}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{amsmath,amsfonts,amssymb,amsthm, gensymb}
\usepackage{color, array, threeparttable}
\usepackage[utf8]{inputenc}
\usepackage{subcaption}
\usepackage{tikz}
\renewcommand{\arraystretch}{1.2}
\usepackage[english]{babel}

\usepackage{booktabs,caption}
\usepackage{hyperref}
\usepackage{floatrow}
\usepackage{cite}
\DeclareMathAlphabet{\mathbbold}{U}{bbold}{m}{n}    
\usepackage{makecell}

\floatsetup[table]{capposition=top}
\usepackage[font=small]{caption,subcaption}
%\usepackage{mwe}



\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage[ruled,boxed]{algorithm2e}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\begin{document}
\section*{Modelling intra- and inter-node MPI communication on AMD Epyc 7601, ARM Cavium ThunderX2 CN9980, Intel Xeon Gold and ARM Kunpeng 920-6428}
The heterogeneity of state-of-the-art hardware architectures can be an obstacle to achieving optimal performance with parallel applications. Hardware layout and process placement may have an impact on both inter-and intra-node MPI communication performance. Models for estimating communication costs are useful for understanding and potentially improving MPI performance. Traditional models for evaluating the point-to-point MPI communication do not take the heterogeneous architecture into account, and therefore yield unsatisfactory results when applied to modern platforms. In addition, intra-node MPI performance is often less emphasised, since for most applications, inter-node communication represents the largest potential for a bottleneck. In this note we will present a model for intra-node MPI communication cost aimed at heterogeneous computing architectures. We consider four such architectures, the AMD EPYC 7601, ARM Cavium ThunderX2 CN9980, Intel Xeon Gold 6140 processors and ARM Kunpeng 920-6428.
\subsection*{Hardware description}
\subsubsection*{AMD Epyc 7601}
The dual AMD EPYC 7601 processor has 64 cores spread over 2 sockets made up of 4 dies. Each of the 8 NUMA domain dies consist of two core complexes (CCXs) with shared L3 caches. This hardware layout gives rise to 4 levels of inter-process connections, intra-CCX, inter-CCX/intra-die, inter-die/intra-socket and inter-socket. We denote these levels respectively as $L_0,L_1,L_2$ and $L_3$. 
\\
\\
There are 16 memory channels in the dual AMD EPYC 7601 processor, two in each die. The theoretical peak bandwidth for the entire node is 341.6 GB/s and 42.7 GB/s for the individual dies. In Table \ref{tab:stream} results from the STREAM bandwidth benchmark (Only copy operation) are displayed. Here we observe a peak bandwidth of 236.4 GB/s when every memory channel is used, and this number represents a more realistic upper bound on the memory bandwidth of an Epyc node. On each socket, the four dies are linked through the AMD Infinity Fabric, which has a theoretical 42.7 GB/s bi-directional bandwidth. Each die also has a dedicated  Infinity Fabric inter-socket link with a theoretical bi-directional bandwidth of 37.9 GB/s. Optimal inter-socket bandwidth therefore require the use of all dies on both sockets. In practice the peak theoretical bandwidth is not attained. 
\\
\\
The Epyc nodes are interconnected with Mellanox HDR InfiniBand (25 GB/s). The Mellanox ConnectX-6 HCA cards are located within one of the nodes NUMA domains/dies, and as we will see the location of the HCA card has a large impact on inter-node MPI performance. On our Epyc nodes the HCA is located on the 5th die, assuming a numbering of the 8 dies from D0 to D7, where D0-D3 are located on socket S0 and D4-D7 on socket S1. 
\subsubsection*{ARM Cavium ThunderX2 CN9980}
The dual Cavium ThunderX2 CN9980 processor nodes also have 64 cores in total, 32 on each socket. Unlike the AMD Epyc nodes, each socket is a single NUMA domain. This means that this node has 2 levels of inter-process connection, intra-socket($L_0$) and inter-socket($L_1$). The theoretical maximal bandwidth of a dual Cavium ThunderX2 node is 317.9 GB/s. In Table \ref{tab:stream}, we observe a peak bandwidth of 222.3 GB/s when both sockets are used in the STREAM benchmark, which is marginally lower than what is attained on the Epyc nodes.
\\
\\
The Cavium nodes are interconnected with Mellanox HDR InfiniBand (25 GB/s). Similar to the Epyc nodes, the NUMA domain/socket location of the Mellanox ConnectX-6 HCA card impacts performance. On our Cavium nodes, the HCA card is attached to socket 0.
\begin{table}[h!]
\caption{Results of copy-STREAM benchmark in GB/s on the Epyc, Cavium ThunderX2, Intel Xeon and ARM Kunpeng nodes. }
\label{tab:stream}
\begin{tabular}{| l | l | l | l | l | l |}
\hline
Domain & Channels &  AMD Epyc   & ARM Cavium   & Intel Xeon  & ARM Kunpeng \\
\hline
Die & 2&  29.8 & -- & -- & -- \\
Socket & 8/$6^*$  & 119.2 & 110.5 & $86.5^*$ & 126.0 \\
Node & 16/$12^*$& 236.4 & 222.3 & $167.6^*$ & 250.0 \\
\hline 
\end{tabular}
\end{table}
\subsubsection*{Intel Xeon Gold 6140}
The dual processor Intel Xeon Gold 6140 has 32 cores in total, and 12 memory channels adding up to a total theoretic maximal bandwidth of 238.42 GB/s, which is lower than both the Epyc and Cavium nodes. From the STREAM results in Table \ref{tab:stream}, we observe a peak bandwidth of 167 GB/s, a lower result than what is achieved on the Epyc and Cavium. The full node has two levels of inter-process connections, intra-socket($L_0$) and inter-socket($L_1$).
\\
\\
As with the Epyc and Cavium nodes, the Intel nodes are interconnected with Mellanox ConnectX-6 InfiniBand HCA cards. However, in contrast to the Cavium and Epyc nodes, the Intel Xeon HCA card location does not impact inter-node MPI performance.
\subsubsection*{ARM Kunpeng 920-6428}
The dual process Kunpeng 920-6428 has 128 cores and 16 memory channels. The theoretical bandwidth of the ARM Kunpeng 920 is 381.4 GB/s. This is a higher bandwidth than on the other platforms, which is again reflected in the STREAM results for the Kunpeng nodes presented in Table \ref{tab:stream}. We here observe a peak of 250 GB/s when both sockets are used. The Dual process Kunpeng consists of two sockets and therefore has two levels of intra-node inter-process connections: Intra socket ($L_0$) and inter socket ($L_1$).
\subsection*{Modified OSU benchmark}
To investigate the intra-node point-to-point MPI performance on the different computing platforms, and the impact of process placement, we use a ping-pong benchmark. This benchmark is described in Algorithm \ref{alg:pp}, and is a modification of the OSU bibw benchmark. The benchmark is a bi-directional ping-pong test, where messages are sent between $N$ pairs of processors. The ping-pong benchmark differs from the osu\_bibw in that it allows more than a single pair. In addition we report execution time instead of bandwidth, since we are also interested in latency. We also consider the case where the message size vary between pairs. This is achieved by including a size scaler $\Gamma_k, =0,...,N-1$, associated with each pair.

\begin{algorithm}[h!] 
\KwData{rank,$N$,max\_size,$size\_scaler$,$\{\Gamma_k\}_{k=0}^{N-1}$}
\For{$s<max\_size$}{
\eIf{$rank<N$}{
$k=rank$\;
\For{$i<n$}{
MPI\_Isend(sdata,$\Gamma_{k}$s,$rank+N$)\;
}
\For{$i<n$}{
MPI\_Irescv(rdata,$\Gamma_{k}$s,$rank+N$)\;
}
MPI\_Waitall()\;
}{
$k=\frac{rank}{2}$\;
\For{$i<n$}{
MPI\_Irescv(rdata,$\Gamma_{k}$s,$rank-N$)\;
}
\For{$i<n$}{
MPI\_Isend(sdata,$\Gamma_{k}$s,$rank-N$)\;
}
MPI\_Waitall()\;
}
$s=roof(s\cdot size\_scaler)$\;
}
\caption{Ping-pong benchmark \label{alg:pp}}
\end{algorithm}
\noindent
\\
To see how process placement impact performance we can run the ping-pong benchmark with different number of pairs, and with ranks pinned to specific NUMA domains on the AMD and ARM nodes. If we for example wish to measure the inter-socket bandwidth, with a single pair, we would map rank 0 and 1 to cores on different sockets. 
\subsection*{Model}
The traditional model for estimating the cost of point-to-point communication, when sending a single message of $s$ bytes, is based on a latency parameter $\tau$ and a per-byte data transfer cost parameter $\alpha$:
\begin{align}
T = \tau + \alpha s \label{eq:post}
\end{align} 
We base the execution time model $T_L(N,s)$ of the ping-pong benchmark in Algorithm \ref{alg:pp} Eq. \ref{eq:post}. Let $T_L(N,s)$ be an expression for the cost of bi-directionally sending $s$ bytes between $N$ pairs of the same level $L$ and type. In this case latency and per-byte data transfer cost (or rather bandwidth) depends on the level $L$:
%We assume that the execution time $T_L(N,s)$ of bi-directionally sending data of size $s$ between $N$ pairs of the same type $L$ and the same NUMA domains is: 
\begin{align}
T_L(N,s) = \tau_L + \frac{2Ns}{BW_L(N)}. \label{eq:Tmod}
\end{align}
Note that two pairs are only of the same type if they involve the same NUMA-domains. For example on the Epyc nodes, two $L_0$ pairs are only the same type if they are mapped to the same die. The model execution time depends on the size of the message, as well as the latency and bandwidth parameters $\tau_L$ and $BW_L(N)$. These parameters again depend on the pair level $L$ and the number of pairs (or processes) used. $BW_L(N)$ depends on a base bandwidth $b_L$ that scales with the number of pairs $N$, and a an upper bandwidth parameter $B_L$:
\begin{align}
BW_L(N) = min(Nb_L, B_L). \label{eq:BWmod}
\end{align}
%This means that we for each level need to estimate three parameters, $\tau_L$, $b_L$ and $B_L$.
The expression in Eq. \ref{eq:Tmod} is valid for multiple pairs of the same connection type and level. The more interesting case is when there is a mix of pairs of different types. To model this mixing of pairs, we assume that transferring data within a NUMA-domain and between NUMA-domains are independent of one another. We again consider the execution time of the ping-pong benchmark in Algorithm \ref{alg:pp}, but now we have $j$ different types of pairs $L_{m_1},...,L_{m_j}$. If all pairs send the same amount of data, and there are $N_{L_{m_i}}$ pairs of type $L_{m_i}$, we model the total execution time as:
\begin{align}
T_{intra}(N_{L_{m_1}},..,N_{L_{m_j}},s) = \max_{i=1,..j} \left( \tau_{L_{m_i}} + \frac{2N_{L_{m_i}}s}{BW_{L_{m_i}}(N_{L_{m_i}})} \right) \label{eq:mix}
\end{align}
A further complication to the model is that MPI uses different protocols based on message size to transfer data. For the openMPI case these protocols are short, eager and rendezvous. This means that there are three sets of latency and bandwidth parameters, one set for each protocol. We will mainly focus on getting our model right for the rendezvous protocol, as we consider this the protocol that targets the most relevant message sizes.
\subsubsection*{Inter-node model}
There are two main differences between inter-node and intra-node data transfer. First, unlike the intra-node case, the inter-node bandwidth does not scale with how many pairs, or processes, that are involved. The second difference is only relevant for the AMD Epyc and ARM Cavium platforms, where process placement relative to HCA card location impact data transfer bandwidth. On these two platforms the HCA card impact creates pair levels similar to the intra-node case. However, for the inter-node case, pairs on different levels are not independent. The total data transfer bandwidth will depend on how many and what types of pairs are involved. Unfortunately, as we will see, how pairs of different inter-node levels impact total bandwidth can not be generalised to a single formula that holds for both the Epyc and Cavium nodes. The inter-node data transfer bandwidth $BW_{inter}$ is therefore a function of the inter-node level of each pair.
\\
\\
An inter-node data transfer cost model $T_{inter}$ for bi-directionally sending s bytes of data with $N_i$ pairs of type $L_i$ is then:
\begin{align}
T_{inter}(N_{L_{1}},..,N_{L_{j}},s) = \max_{i=1,...,j}\tau_{L_i} + \frac{\sum_{i=1}^j 2N_{L_{i}}s}{BW_{inter}(N_{L_{1}},..,N_{L_{j}})}. \label{eq:interTotalMod}
\end{align}
\subsubsection*{Non-uniform message size}
The expression in Eq. \ref{eq:Tmod} and \ref{eq:mix} assumes that every pair exchanges a message of equal length $s$. Because uniform message sizes are unrealistic in actual application, we want to extend Eq. \ref{eq:Tmod} and \ref{eq:mix} to allow pairs exchanging different message sizes. Assume that we have $N$ pairs of the same type $L$, bi-directionally sending $S=\{s_1,...,s_N\}$ bytes. If the message sizes $s_i,s_j$ are ordered such that $s_i\leq s_j$ if $i\leq j$, the extended data transfer cost model $T_L(N,S)$ based on Eq. \ref{eq:Tmod} is:
\begin{align}
T_L(N,S) = \tau_L + \sum_{i=0}^{N-1}\frac{2(N-i)(s_{i+1}-s_{i})}{BW_L(N-i)},\quad s_0=0. \label{Tmod2}
\end{align}
Eq. \ref{Tmod2} reflects that overall bandwidth is affected by the number of active MPI-processes, and that one pair has finished its data transfer, while others have not, the total bandwidth will potentially drop. If we have a mix of $j$ pair types $L_{m_1},...,L_{m_j}$, and for each pair type $i$ there are $N_{i}$ pairs that send/receive $S_i=\{s_1,...,s_{N_i}\}$ bytes, the total cost model would be:
\begin{align}
T_{intra}(N_{L_{1}},..,N_{L_{j}},S_1,...,S_{j}) = \max_{i=1,..j} \left( T_{L_{m_i}}(N_{L_i},s_i) \right).
\end{align}
The inter-node cost model in Eq. \ref{eq:interTotalMod} can also be extended to include non-uniform messages. The inter-node case is especially easy if one assumes that all inter-node pairs are of the same type, since the the total bandwidth then is independent of number of pairs:
\begin{align}
T_{inter}(N,S) = \tau_L + \frac{\sum_{i=1}^{N}2s_{i}}{BW_{inter}}. \label{interTmod2}
\end{align}
\subsection*{Estimation of parameters}
We estimate the parameters in Eq. \ref{eq:Tmod}-\ref{eq:BWmod}, by first running our ping-pong benchmark in Algorithm \ref{alg:pp} with increasing message sizes using one pair for each level. Linear regression can then be used to estimate $\tau_L$ and $b_L$. To find $B_L$, we run the benchmark with more pairs, and observe how the bandwidth develops. $B_L$ is then set as the largest achieved bandwidth when using more than one pair on the same level. The measured values are presented in Table \ref{tab:amd&arm}. How the estimated $\tau_L$ and $b_L$ match with measured execution times for single pair runs of the ping-pong benchmark in Algorithm \ref{alg:pp} is displayed in Figure \ref{fig:pair1}.
\begin{table}[h!]
\caption{Estimated rendezvous protocol latency $\tau_L$ and bandwidth parameters $b_L$, $B_L$ for model Eq \ref{eq:Tmod} on AMD Epyc 7601, ARM Cavium ThunderX2, Intel Xeon Gold 6140 and ARM Kunpeng 920-6428. For the Cavium, intel Xeon and Kunpeng nodes the $L_0$ level represents data transfers within the same socket, while the $L_1$ level is inter-socket data transfer. On the Epyc nodes the levels mean: intra-CCX ($L_0$), intra-die and inter-CCX ($L_1$), inter-die and intra-socket ($L_2$) and inter-socket ($L_3$).}
\label{tab:amd&arm}
\begin{tabular}{| l | r | r | r | r | r | r | r | r | r | r | r | r |}
\hline
& \multicolumn{3}{l |}{AMD Epyc } & \multicolumn{3}{l |}{ARM Cavium } & \multicolumn{3}{l |}{Intel Xeon } & \multicolumn{3}{l |}{ARM Kunpeng}\\
\hline
Level & $\tau_L$ & $b_L$ & $B_L$  & $\tau_L$& $b_L$ & $B_L$  & $\tau_L$& $b_L$ & $B_L$ & $\tau_L$ & $b_L$ & $B_L$\\
\hline
$L_0$ &10us & 21.2& 27& 8.2us & 12.8& 103.1 & 20us &11.0 & 63.2& 24.4us & 8.0& 105.9\\
$L_1$ &13us & 9.4& 14.7& 12us & 7.6& 60.3 & 25us &7.0 & 51.7& 27.7& 4.8& 80.6\\
$L_2$ &16us & 6.7& 11.2& -& -&- &- &- &-&-&-&-\\
$L_3$ & 21us& 5& 9.14&- & -&- &- &- &-&-&-&-\\
\hline
\end{tabular}
\end{table}
\\
Note that the $B_L$ parameter is much larger on Cavium, Intel and Kunpeng than on Epyc. This is because the $L_0$ on the ARM and Intel nodes consists of an entire socket that contains 6 or 8 memory channels, while $L_0$ on the AMD Epyc node correspond only to a single die equipped with 2 memory channels. This also means that on the the Epyc nodes the bandwidth scales with the number of involved dies. 

\begin{figure}[h!]
\begin{subfigure}{.5\textwidth}
%left,botom,right,top
\includegraphics[scale=0.25,trim={1.7cm 0cm 2.8cm 2cm},clip]{figures/epycBetaTau.png}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.25,trim={1.7cm 0cm 2.8cm 2cm},clip]{figures/caviumBetaTau.png}
\end{subfigure}
\caption{Plots display execution time of Algorithm \ref{alg:pp} on all levels with a single pair on AMD Epyc and ARM Cavium nodes. Bandwidth and latency calculated using regression added to plot.}
\label{fig:pair1}
\end{figure}
\subsection*{Inter-node bandwidth}
We report the inter-node bandwidth on the AMD Epyc and ARM Cavium nodes in Figure \ref{fig:interBWs}. For the AMD Epyc results in Figure \ref{fig:interBWs}a the bandwidth is displayed for all combinations of process mappings to the 8 dies D1,...,D7. We observe that mapping processes to die D5, where the HCA card is located yields a bandwidth close to the theoretical Mellanox HDR InfiniBand bi-directional bandwidth of 25 GB/s. The bandwidth measurements are significantly lower, at ca. 15 GB/s, when processes are instead mapped to the die D4, D5 and D6 on the same socket as D5. Inter-node data transfer bandwidth is further reduced if one maps one or both processes to the D0, D1, D2 and D3 dies located on a different socket than D5.   Here we observe a bandwidth between 6.4 and 7.4 GB/s.
\\
\\
We observe similar results on the ARM Cavium nodes. Mapping both processes to  socket S0, where the HCA card is located, yields a bandwidth of 21.3 GB/s, and placing one or both process to socket S1 results in reduction in the inter-node bandwidth to between 12.9 and 13.4 GB/s.
\begin{figure}[h!]
\begin{subfigure}{.5\textwidth}
%left,botom,right,top
\includegraphics[scale=0.3]{figures/8x8InterEpycBW.png}
\caption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.3]{figures/2x2CaviumInterBW.png}
\caption{}
\end{subfigure}
\caption{Bi-directional inter-node bandwidth on the AMD Epyc (a) and ARM Cavium (b) nodes. The D0,...,D7 on x- and y-axis represents the die placement of each process when running on AMD Epyc. Similarly, S0 and S1 denotes the sockets on the Cavium nodes.}
\label{fig:interBWs}
\end{figure}
\section*{Intra-node model test}
In this section we will demonstrate the intra-node model in Eq. \ref{eq:mix} on Epyc and Cavium. All experiments are done using the ping-pong benchmark in Algorithm \ref{alg:pp}. To test that the model works, we run the ping-pong benchmark with different number of processes and process mappings.
\subsection*{AMD Epyc experiments}
To test the data transfer cost model in Eq. \ref{eq:mix} on the AMD Epyc we do several tests. First we look at how the model performs when we run the ping-pong benchmark with multiple pairs of the same type. Then we consider multiple pairs of level $L_3$ on different dies to showcase inter-socket performance. Lastly we run the ping-pong benchmark with pairs of different types. The details of the experiments are displayed in Table \ref{tab:expEpyc}. 
\begin{table}[h!]
\caption{This table presents experiments on the AMD Epyc node using the ping-pong benchmark in Algorithm \ref{alg:pp}. The results of these experiments, that aim to compare measurements to the data transfer cost model in Eq. \ref{eq:Tmod}, are presented in Figure \ref{fig:epycSameLevel}-\ref{fig:epycMixLevel}.}
\label{tab:expEpyc}
\begin{tabular}{| c | c | c | c | c |}
\hline
Test & \# pairs & Pair mapping & Levels & Figure \\
\hline
1 & 4& 4 pairs of D0 $\longleftrightarrow$ D1& 4 $L_2$ pairs & Figure \ref{fig:epycSameLevel}a\\
 \hline
2 & 8 & 8 pairs of D1 $\longleftrightarrow$ D4& 8 $L_2$ pairs & Figure \ref{fig:epycSameLevel}b \\
 \hline
3 & 16 &\makecell{8 pairs of D1 $\longleftrightarrow$ D5 \\ 8 pairs of D2 $\longleftrightarrow$ D6} & 16 $L_2$ pairs &Figure \ref{fig:epycIntraSocket}\\
 \hline
4 & 6& \makecell{2 pairs of D1 $\longleftrightarrow$ D1 \\ 2 pairs of D4 $\longleftrightarrow$ D4 \\ 2 pairs of D2 $\longleftrightarrow$ D3}& \makecell{2 $L_0$ pairs \\ 2 $L_1$ pairs \\ 2 $L_2$ pairs} & Figure \ref{fig:epycMixLevel}a\\
 \hline
5 & 8 & \makecell{4 pairs of D1 $\longleftrightarrow$ D1 \\ 2 pairs of D6 $\longleftrightarrow$ D7 \\ 1 pair of D2 $\longleftrightarrow$ D4 \\ 1 pair of  D3 $\longleftrightarrow$ D5 } & \makecell{4 $L_0$ pairs \\ 2 $L_2$ pairs \\ 2 $L_3$ pairs}& Figure \ref{fig:epycMixLevel}b\\
% \hline
%6 & & & & \\
 \hline
\end{tabular}
\end{table}
\\
\\
The results of test 1 and 2 as presented in Table \ref{tab:expEpyc} are displayed in Figure \ref{fig:epycSameLevel}. For these tests the ping-pong benchmark was run with processes mapped so that messages were sent between dies on the same socket (Figure \ref{fig:epycSameLevel}a) or between dies on different sockets (Figure \ref{fig:epycSameLevel}b). We observe that the measurements fit with the model. In the plot in Figure \ref{fig:epycIntraSocket} the result from test 3 is presented. Here the ping-pong benchmark were run using on 16 intra-socket pairs. The processes were mapped so that only two of four dies on both sockets were used. We again observe that the model fits the measurements. This also demonstrates how inter-socket data transfer scales with the number of dies used.
\\
\\
In test 4 and 5 we ran the ping-pong benchmark with pairs of differing levels. The results are displayed in Figure \ref{fig:epycMixLevel} and how the processes were mapped is explained in Table \ref{tab:expEpyc}. The model only aims to match the slowest pair, and the plots in Figure \ref{fig:epycMixLevel} show that the model manages to do this for test 4 and 5.
\begin{figure}[h!]
\begin{subfigure}{.55\textwidth}
%left,botom,right,top
\includegraphics[scale=0.3,trim={1.7cm 0cm 2.8cm 2cm},clip]{figures/P4EpycL2.png}
\caption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.3,trim={1.7cm 0cm 2.8cm 2cm},clip]{figures/P8EpycL3.png}
\caption{}
\end{subfigure}
\caption{The plots in this figure are the results of AMD Epyc test 1 and 2 detailed in Table \ref{tab:expEpyc}. The plots display the execution time of each pair in the ping-pong benchmark, as well as the data transfer cost model estimate.}
\label{fig:epycSameLevel}
\end{figure}
\begin{figure}[h!]
\includegraphics[scale=0.4]{figures/P16EpycD1D2D5D6L3.png}
\caption{The plot in this figure are the results of AMD Epyc test 3 detailed in Table \ref{tab:expEpyc}. The plot displays the execution time of each pair in the ping-pong benchmark, as well as the data transfer cost model estimate.}
\label{fig:epycIntraSocket}
\end{figure}
\begin{figure}[h!]
\begin{subfigure}{.55\textwidth}
%left,botom,right,top
\includegraphics[scale=0.3,trim={1.7cm 0cm 2.8cm 2cm},clip]{figures/mixP6EpycL0L1L2.png}
\caption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.3,trim={1.7cm 0cm 2.8cm 2cm},clip]{figures/mixP8Epyc4022.png}
\caption{}
\end{subfigure}
\caption{The plots in this figure are the results of AMD Epyc test 4 and 5 detailed in Table \ref{tab:expEpyc}. The plots display the execution time of each pair in the ping-pong benchmark, as well as the data transfer cost model estimate.}
\label{fig:epycMixLevel}
\end{figure}
\subsection*{ARM Cavium experiments}
We present some results attained on the Cavium nodes. In Figure \ref{fig:armBWmodelB} the measured bandwidth achieved with the ping-pong benchmark is plotted for level $L_0$ and $L_1$ against increasing number of pairs. The expected bandwidths based on Eq. \ref{eq:BWmod} for the two levels are also included in the plot. We notice that the model overestimates the bandwidth when using between 8 and 14 pairs, especially for level $L_0$.
\\
\\
In Figure \ref{fig:armMixLevel} we present results from runs of the ping-pong benchmark where ran with pairs of both levels. The plot in Figure \ref{fig:armMixLevel}a displays execution time of two pairs, one intra-socket and one inter-socket. The plot in \ref{fig:armMixLevel}b is similar, but instead has 4 of each pair.
\begin{figure}[h!]
\includegraphics[scale=0.4]{figures/BetaBmodel.png}
\caption{The measured and estimated bandwidth achieved on the ARM Cavium nodes, when running the ping-pong benchmark with increasing number of MPI processors. The plot display both intra- and inter-socket bandwidth.}
\label{fig:armBWmodelB}
\end{figure}

\begin{figure}[h!]
\begin{subfigure}{.55\textwidth}
%left,botom,right,top
\includegraphics[scale=0.3,trim={1.7cm 0cm 2.8cm 2cm},clip]{figures/caviumMix1-1P2.png}
\caption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.3,trim={1.7cm 0cm 2.8cm 2cm},clip]{figures/caviumMix4-4P8.png}
\caption{}
\end{subfigure}
\caption{The plots in this figure are the results of running the ping-pong benchmark on the ARM Cavium nodes. The plots display the execution time of each pair, as well as the data transfer cost model estimate. In (a) there are two pairs, one intra-socket and one inter-socket. In (b) a total of 8 pairs are used, with 4 of each type.}
\label{fig:armMixLevel}
\end{figure}
\end{document}