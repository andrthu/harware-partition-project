\documentclass{article}
\usepackage[a4paper]{geometry}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{amsmath,amsfonts,amssymb,amsthm, gensymb}
\usepackage{color, array, threeparttable}
\usepackage[utf8]{inputenc}
\usepackage{subcaption}
\usepackage{tikz}
\renewcommand{\arraystretch}{1.2}
\usepackage[english]{babel}

\usepackage{booktabs,caption}
\usepackage{hyperref}
\usepackage{floatrow}
\usepackage{cite}
\DeclareMathAlphabet{\mathbbold}{U}{bbold}{m}{n}    
\usepackage{makecell}

\floatsetup[table]{capposition=top}
\usepackage[font=small]{caption,subcaption}
%\usepackage{mwe}



\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage[ruled,boxed]{algorithm2e}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\begin{document}
\section*{Modelling Intra-node communication}
The traditional model for estimating the cost of MPI point-to-point communication, when sending a single message of $s$ bytes, is based on a latency parameter $\tau$ and a per-byte data transfer cost parameter $\alpha$:
\begin{align}
T = \tau + \alpha s \label{eq:post}
\end{align} 
In the intra-node case, the latency $\tau$ and the data transfer cost, or bandwidth, $\alpha$ depends on where the relative hardware location of the processes, the available bandwidth resources of the active processes and the MPI protocol. The MPI protocol depends on the message size $s$. We model the cost $T_{L,P}(N,s)$ of $N$ similarly mapped process pairs bi-directionally sending messages of size $s$ using the same protocol as:
\begin{align}
T_{L,P}(N,s) = \tau_{L,P} + \frac{2Ns}{BW_{L,P}(N)}. \label{eq:Tmod}
\end{align}
Here, $P$ denotes protocol, while $L$ is the pair level or type. The latency $\tau_{L,P}$ and the bandwidth $BW_{L,P}(N)$ depend on the protocol and pair level. The bandwidth also depends on the number of pairs involved in sending messages. The reason for this, is that on many architectures, multiple processes are required to fully saturate the memory access bandwidth. We model the bandwidth $BW_{L,P}(N)$ with two parameters $b_{L,P}$ and $B_{L,P}$:
\begin{align}
BW_{L,P}(N) = min(Nb_{L,P}, B_{L,P}). \label{eq:BWmod}
\end{align}
$b_{L,P}$ is a base bandwidth that scales with the number of pairs $N$ used up till a maximal bandwidth $B_{L,P}$.
\subsection*{Estimating model parameters on the ARM Cavium ThunderX2 CN9980 node}
The dual ARM Cavium nodes have 64 cores placed on two sockets. Each socket is a single NUMA domain, and a point-to-point message sending pair can therefore either be of two types, intra socket or inter-socket. Using a ping-pong benchmark and regression we estimate the latency and bandwidth parameters $\tau_{L,P}, b_{L,P}$ and $B_{L,P}$ for the eager and rendezvous protocols. These results are presented in Table \ref{tab:cavium1}. Note that the eager protocol is used when sending messages lower than 8K bytes, and consequently the rendezvous protocol is used when messages are larger than or equal to 8K bytes.
\\
\begin{table}[h!]
\caption{Latency and bandwidth parameters for the transfer cost model in Eq. \ref{eq:Tmod} for the eager and rendezvous protocols on the ARM Cavium ThunderX2 CN9980 node. }
\label{tab:cavium1}
\begin{tabular}{| l | r | r | r | r | r | r |}
\hline
Protocol & $\tau_{0,P}$ & $b_{0,P}$ & $B_{0,P}$& $\tau_{1,P}$ & $b_{1,P}$ & $B_{1,P}$\\
\hline
Eager & 0.45us&12.3GB/s&98.4GB/s&0.85us&4.8GB/s&58.8GB/s \\
\hline
Rendezvous &2.2us&14.3GB/s&36.7GB/s&4.9us&15.3GB/s&23.5GB/s \\
\hline
\end{tabular}
\end{table}
\\
From Table \ref{tab:cavium1}, we notice in particular that the maximal bandwidth parameter $B_{L,P}$ is much larger in the eager protocol than in the rendezvous protocol. In Table \ref{tab.cavium2} we present more detailed bandwidth measurements for increasing number of pairs. We observe that the bandwidth caps out much earlier for the rendezvous than the eager protocol. Note that the expression in Eq. \ref{eq:BWmod} is a better fit for eager messages than rendezvous messages.
\begin{table}[h!]
\caption{Measured eager and rendezvous MPI message transfer bandwidth (GB/s) on ARM Cavium ThunderX2 CN9980 nodes against increasing number of pairs.}
\label{tab.cavium2}
\begin{tabular}{| l | r | r | r | r |}
\hline
\# pairs& Eager $BW_0$ &  Eager $BW_1$ & Rendezvous $BW_0$ &  Rendezvous $BW_01$\\
\hline
1&12.0&4.8&12.3&15.3 \\
\hline
2&22.1&9.5&19.6&18.4 \\
\hline
4&43.4&18.8&31.4&22.7 \\
\hline
8&68.1&36.7&36.7&23.1 \\
\hline
16&98.4&58.8&32.9&23.5 \\
\hline
\end{tabular}
\end{table}
\section*{Modelling intra-node non-uniform message size communication}
The expression in Eq. \ref{eq:Tmod} assumes that every pair exchanges a message of equal length $s$. Because uniform message sizes are unrealistic in actual application, we want to extend Eq. \ref{eq:Tmod} to allow pairs exchanging different message sizes. Assume that we have $N$ pairs of the same type $L$, bi-directionally sending $S=\{s_1,...,s_N\}$ bytes. For simplicity, we also assume that all messages will result in similar use of protocols. If the message sizes $s_i,s_j$ are ordered such that $s_i\leq s_j$ if $i\leq j$, the extended data transfer cost model $T_{L,P}(N,S)$ based on Eq. \ref{eq:Tmod} is:
\begin{align}
T_{L.P}(N,S) = \tau_{L,P} + \sum_{i=0}^{N-1}\frac{2(N-i)(s_{i+1}-s_{i})}{BW_{L,P}(N-i)},\quad s_0=0. \label{eq:Tmod2}
\end{align}
Eq. \ref{eq:Tmod2} reflects that overall bandwidth is affected by the number of active MPI-processes, and that one pair has finished its data transfer, while others have not, the total bandwidth will potentially drop. An example of the model in Eq. \ref{eq:Tmod2} is displayed in Figure \ref{fig:varSize}.
\begin{figure}[h!]
\includegraphics[scale=0.3]{figures/L0T8.png}
\caption{Measured and modelled execution time of ping-pong benchmark, with 4 intra-socket pairs on the ARM Cavium node against message size. One of the pairs always send messages 2 times larger than the message size of the other pairs.}
\label{fig:varSize}
\end{figure}
\end{document}