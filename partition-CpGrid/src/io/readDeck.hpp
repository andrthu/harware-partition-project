#include <opm/parser/eclipse/EclipseState/Grid/TransMult.hpp>
#include <opm/parser/eclipse/Parser/ErrorGuard.hpp>
#include <opm/grid/cpgrid/CartesianIndexMapper.hpp>


class ReadProperties {
private:
    Opm::Parser parser_;
    Opm::ParameterGroup param_;
    
    std::shared_ptr<Opm::Deck> deck_;
    std::shared_ptr<Opm::EclipseState> eclipse_state_;
    std::shared_ptr<Opm::Schedule> schedule_;

    std::vector<double> trans_;
    std::vector<double> perm_;

    Dune::BlockVector<Dune::FieldVector<double,1>> weights;
    Dune::BlockVector<Dune::FieldVector<double,1>> flow_face_pattern;
    
    std::vector<Dune::FieldMatrix<double,3,3>> perm2_;
    void calcPerm_(const Dune::CpGrid& grid)
    {
	//const auto& props = eclipse_state_->get3DProperties();
	const auto& props = eclipse_state_->fieldProps();
	unsigned numCells = grid.numCells();
	perm2_.resize(numCells);

	/*
	const std::vector<double>& permxData = props.getDoubleGridProperty("PERMX").getData();
	const std::vector<double>& permyData = props.getDoubleGridProperty("PERMY").getData();
	const std::vector<double>& permzData = props.getDoubleGridProperty("PERMZ").getData();
	*/
	const std::vector<double>& permxData = props.get_global_double("PERMX");
	const std::vector<double>& permyData = props.get_global_double("PERMY");
	const std::vector<double>& permzData = props.get_global_double("PERMZ");
	
	Dune::CartesianIndexMapper<Dune::CpGrid> cartmap(grid);
	
	for (unsigned dofIdx = 0; dofIdx < numCells; ++ dofIdx) {
	    unsigned cartesianElemIdx = cartmap.cartesianIndex(dofIdx);
	    perm2_[dofIdx] = 0.0;
	    perm2_[dofIdx][0][0] = permxData[cartesianElemIdx];
	    perm2_[dofIdx][1][1] = permyData[cartesianElemIdx];
	    perm2_[dofIdx][2][2] = permzData[cartesianElemIdx];
	}
    }
    void multNTG(const Dune::CpGrid& grid, Opm::EclipseState& eclState, 
		 std::vector<double>& ntg, const std::vector<double>& porv, 
		 const std::vector<int>& actnum)
    {
	
	auto eclgrid = eclState.getInputGrid();
	const int * cartdims = Opm::UgGridHelpers::cartDims(grid);
	
	auto gid = grid.globalIdSet();
	auto gridView = grid.leafGridView();
	
	//const int * globCell = Opm::UgGridHelpers::globalCell(grid);
	const int * globCell =Opm::UgGridHelpers::globalCell(grid);
	
	for (int cellItr = 0; cellItr < grid.numCells(); ++cellItr)
	{
	    int cell = globCell[cellItr];//gid.id(e);

	    const int nx = cartdims[0];
	    const int ny = cartdims[1];
	    const double volume = eclgrid.getCellVolume(cell);

	    ntg[cell] *= volume;
	    double totalCellVolume = volume;
	    
	    int upperCell = cell - nx*ny;
	    while(upperCell > 0  && actnum[upperCell] > 0 && porv[upperCell] < eclgrid.getMinpvVector()[upperCell])
	    {
		const double upperVolume = eclgrid.getCellVolume(upperCell);
		totalCellVolume += upperVolume;
		ntg[cell] += ntg[upperCell]*upperVolume;
		
		upperCell -= nx*ny;
	    }
	    ntg[cell]/= totalCellVolume;
	}
    }

    void calcTrans_(const Dune::CpGrid& grid, Opm::EclipseState& eclState, std::vector<double> ntg)
    {
	auto eclGrid = eclState.getInputGrid();
	auto gridView = grid.leafGridView();

	const auto & mult = eclipse_state_->getTransMult();
	trans_.resize(grid.numFaces(), 0.0);
       
	auto cell2Face = Opm::UgGridHelpers::cell2Faces(grid);
	
	std::vector<double> htran(grid.numFaces(), 0.0);
	std::vector<double> vmult(grid.numFaces(), 1.0);
	

	const int * globCell = Opm::UgGridHelpers::globalCell(grid);
	auto faceCells = Opm::UgGridHelpers::faceCells(grid);
	//auto faceCells = Opm::AutoDiffGrid::faceCells(grid);

	for (int cell = 0; cell < grid.numCells(); ++cell)
	{
	    int cellForMult = globCell[cell];
	    auto cellCenter = eclGrid.getCellCenter(cellForMult);
	    
	    const auto faceRow = grid.cellFaceRow(cell);
	    auto faceRange = cell2Face[cell];

	    for (auto it=faceRange.begin(); it!=faceRange.end();++it)
	    {
		const int tag=grid.faceTag(it);
		Opm::FaceDir::DirEnum faceDir;
		if (tag == 0)
		{
		    faceDir = Opm::FaceDir::XMinus;
		}
		else if (tag == 1)
		{
		    faceDir = Opm::FaceDir::XPlus;
		}
		else if (tag == 2)
		{
		    faceDir = Opm::FaceDir::YMinus;
		}
		else if (tag == 3)
		{
		    faceDir = Opm::FaceDir::YPlus;
		}
		else if (tag == 4)
		{
		    faceDir = Opm::FaceDir::ZMinus;
		}
		else if (tag == 5)
		{
		    faceDir = Opm::FaceDir::ZPlus;
		}
		int face = *it;
		int d = std::floor(tag/2);
		
		const auto& faceCenter = Opm::UgGridHelpers::faceCenterEcl(grid,cell,tag);
		const auto& faceNormal = Opm::UgGridHelpers::faceAreaNormalEcl(grid,face);
		
		double faceArea = grid.faceArea(face);
		
		const int faceCell0 = grid.faceCell(face, 0);
		const int faceCell1 = grid.faceCell(face, 1);
		
		bool inside = faceCell0==cell;
		
		const int neighbor = faceCell0==cell ? faceCell1 : faceCell0;
		
		if (true)//neighbor!=-1)
		{
		    const int nab = neighbor;
		
		    double val  = 0.0;
		    double dist = 0.0;
		    for (int k = 0; k < 3; ++k)
		    {
			const double Ci = faceCenter[k]-cellCenter[k];
			dist += Ci*Ci;
			val += Ci*faceNormal[k];
		    }
		    //long double T_in = perm[cell*9+d]*std::abs(val)/dist;
		    long double T_in = perm2_[cell][d][d]*std::abs(val)/dist;
		    if (tag < 4)
		    {
			T_in *= ntg[cellForMult];
		    }
		    if (T_in != 0)
			htran[face] += 1.0/T_in;
		    else
		    {
			vmult[face] = 0;
		    }
		    
		    vmult[face]*= mult.getMultiplier(cellForMult, faceDir);
		    
		    const int cellIn = faceCells(face,0);
		    const int cellOut = faceCells(face,1);
		    
		    int globIn = globCell[cellIn];
		    int globOut = globCell[cellOut];
		    
		    if (inside && nab != -1)
		    {
			vmult[face] *= mult.getRegionMultiplier(cellForMult,globCell[nab],faceDir);
		    }
		}
	    }
	}
	for (unsigned i = 0; i < grid.numFaces(); ++i) {
	    if (htran[i]!=0)
		trans_[i] = vmult[i] * 1.0/htran[i];
	}
    }

public:
    
    void setFlowPattern(Dune::BlockVector<Dune::FieldVector<double,1>> val)
    {
	flow_face_pattern = val;
    }

    Dune::BlockVector<Dune::FieldVector<double,1>> getFaults()
    {
	return weights;
    }

    const double *  getTransmissibility()
    {
	return trans_.data();
	//return geology_->transmissibility().data();
    }

    int getTransLength()
    {
	return trans_.size();
    }

    std::vector<double> getTransVec()
    {
	return trans_;
    }

    const double * getPermeability()
    {
	return perm_.data();
    }

    Opm::Schedule getSchedule()
    {
	return *schedule_;
    }
    
    std::vector<Opm::Well> getWells()
    {
	return schedule_->getWellsatEnd();
    }
    
    std::vector<double> getNTG()
    {
	return eclipse_state_->fieldProps().get_global_double("NTG");
	//return eclipse_state_->get3DProperties().getDoubleGridProperty("NTG").getData();
    }
    const std::vector<double> getPORV()
    {
	return eclipse_state_->fieldProps().get_global_double("PORV");
	//return eclipse_state_->get3DProperties().getDoubleGridProperty("PORV").getData();
    }

    const std::vector<int> getACTNUM()
    {
	return eclipse_state_->fieldProps().actnum();
	//return eclipse_state_->get3DProperties().getIntGridProperty("ACTNUM").getData();
    }
  
    Opm::EclipseState getES()
    {
	return *eclipse_state_;
    }

    Opm::TransMult getTm()
    {
	return eclipse_state_->getTransMult();
    }

    Opm::EclipseGrid getEclGrid()
    {
	return eclipse_state_->getInputGrid();
    }
  

    void uniformBoxInit(Dune::CpGrid grid)
    {
	trans_.resize(grid.numFaces());
	perm_.resize(grid.numCells());
      
	std::fill(trans_.begin(),trans_.end(), 1.0);
	std::fill(perm_.begin(),perm_.end(), 1.0);
    }
  
    void init_grid(Dune::CpGrid& grid, char * deck_name, bool isRoot)
    {
	//declare wells
	std::vector<const Opm::Well*> wells;
	
	const double * trans;
	if(!isRoot)
	{
	    return;
	}
  
	//Create Parameter
	int argc=0;
	char ** argv = 0;

	param_= Opm::ParameterGroup(argc,argv,false,false);

	//Create Parser
	Opm::ParseContext parseContext( { {Opm::ParseContext::PARSE_RANDOM_SLASH,Opm::InputError::IGNORE},
		    {Opm::ParseContext::PARSE_MISSING_DIMS_KEYWORD,Opm::InputError::WARN},
			{Opm::ParseContext::SUMMARY_UNKNOWN_WELL,Opm::InputError::WARN},
			    {Opm::ParseContext::SUMMARY_UNKNOWN_GROUP,Opm::InputError::WARN}} );

	Opm::ErrorGuard errorGuard;
	//create and read input deck with parser
	deck_ = std::make_shared<Opm::Deck>(parser_.parseFile(deck_name, parseContext, errorGuard));
	
	Opm::checkDeck(*deck_,parser_,parseContext,errorGuard);

	//create Eclipse state
	//eclipse_state_.reset(new Opm::EclipseState(*deck_, parseContext, errorGuard));
	eclipse_state_.reset(new Opm::EclipseState(*deck_));
	//Create Scedule, includes wells
	schedule_.reset(new Opm::Schedule(*deck_, *eclipse_state_));	  
					  //parseContext, errorGuard));
  
	//get some propertes from eclipse state
	//const std::vector<double>& porv=eclipse_state_->get3DProperties().getDoubleGridProperty("PORV").getData();
	const auto& porv = eclipse_state_->fieldProps().porv(true); 
	//get grid from deck.
	grid.processEclipseFormat(&(eclipse_state_->getInputGrid()), false, false, 
				  false, porv, eclipse_state_->getInputNNC());

	for (int k=0;k<grid.numCells();++k)
	{
	    std::array<double,3> dims = eclipse_state_->getInputGrid().getCellDims(k);
	}

	std::vector<double> ntg_ = getNTG();
	std::vector<double> ntg(ntg_.size());
	for (unsigned j = 0; j < ntg_.size(); ++j)
	    ntg[j] = ntg_[j];
	//const auto actnum = eclipse_state_->fieldProps().getIntGridProperty("ACTNUM").getData();
	auto actnum = eclipse_state_->fieldProps().actnum();
	
	calcPerm_(grid);
	multNTG(grid, *eclipse_state_, ntg, porv, actnum);
	calcTrans_(grid, *eclipse_state_, ntg);
		
	return;
    }
};


/*
 * Function that creates adjecency pattern of graph representing the grid seen on rank rank.
 */
template<class V>
Dune::MatrixIndexSet getPartitionAdjecency(V vector, Dune::CpGrid grid,int rank,int size,int comTab[])
{
    int numCells = grid.numCells();
    int i,j;
    Dune::MatrixIndexSet op;
    op.resize(numCells,numCells);
  
    //loop over cells of mesh
    for(i=0;i<numCells;++i)
    {
	//check if cell is owned by rank rank
	if(vector[i] == rank)
	{
	    //add vertex
	    op.add(i,i);
	    int numFaces = grid.numCellFaces(i);
	    //loop over cell faces
	    for (j=0;j<numFaces;++j)
	    {
		int faceId = grid.cellFace(i,j);
		
		//get cells that faceId borders.
		//Note that one of the cells is cell i.
		int faceCell0 = grid.faceCell(faceId,0);
		int faceCell1 = grid.faceCell(faceId,1);
	      
		int otherCell = faceCell0==i ? faceCell1:faceCell0;
	      
		//check if cell is outside mesh, i.e. face is on boundary
		if (otherCell!=-1)
		{
		    //check if cell neighbour of cell i is owned by rank rank
		    if (vector[otherCell]==rank)
		    {
			//add edge from cell i to facecell0
			op.add(i,otherCell);
		    }
		    else
		    {
			comTab[rank*size + (int)vector[otherCell]]++;
		    }
		}
	    }
	}
    }
    return op;
}

/*
 * Function that creates adjecency pattern of graph representing the grid seen on rank rank.
 */
template<class V>
Dune::MatrixIndexSet getPartitionAdjecencyWithWells(V vector, Dune::CpGrid grid,Dune::cpgrid::WellConnections wells,int rank,int size,int comTab[],int comTab2[])
{
    int numCells = grid.numCells();
    int i,j;
    Dune::MatrixIndexSet op;
    op.resize(numCells,numCells);

    //loop over cells of mesh
    for(i=0;i<numCells;++i)
    {
	//check if cell is owned by rank rank
	if(vector[i] == rank)
	{
	    //Array to check if current cell has connection with another process.
	    int cellTab[size];
	    for (int k=0;k<size;k++)
		cellTab[k] =0;

	    //add vertex
	    op.add(i,i);
	  
	    int numFaces = grid.numCellFaces(i);
	    //loop over cell faces
	    for (j=0;j<numFaces;++j)
	    {
		int faceId = grid.cellFace(i,j);

		//get cells that faceId borders.
		//Note that one of the cells is cell i.
		int faceCell0 = grid.faceCell(faceId,0);
		int faceCell1 = grid.faceCell(faceId,1);

		//get cell that is connected to faceId, but is not cell i
		int otherCell = faceCell0==i ? faceCell1:faceCell0;
	      
		//check if cell is outside mesh, i.e. face is on boundary
		if (otherCell!=-1)
		{
		    //check if cell neighbour of cell i is owned by rank rank
		    if (vector[otherCell]==rank)
		    {
			//add edge from cell i to facecell0
			op.add(i,otherCell);
		    }
		    else
		    {
			comTab[rank*size + (int)vector[otherCell]]++;
			if (cellTab[(int)vector[otherCell]]==0)
			{
			    comTab2[rank*size + (int)vector[otherCell]]++;
			    cellTab[(int)vector[otherCell]]=1;
			}
		    } //End else
		} // End boundary check
	    } //End loop over cell faces.
	}
    }
  
    for(const auto & well_indicies: wells)
    {
	for(auto well_idx = well_indicies.begin(); well_idx != well_indicies.end();++well_idx)
	{
	    if (vector[*well_idx]==rank)
	    {
		auto well_idx2 = well_idx;
		for( ++well_idx2; well_idx2 != well_indicies.end();
		     ++well_idx2)
		{
		    if (vector[*well_idx2]==rank)
		    {
			op.add(*well_idx,*well_idx2);
			op.add(*well_idx2,*well_idx);
		    }
		}
	    }
	}
    }
  
    return op;
}

//function for printing a sparse matrix
template<class M>
void printMatrix(M m, int numCells)
{
    int i,j;

    for (i=0;i<numCells;i++)
    {
	for(j=0;j<numCells;j++)
	{
	    if (m.exists(i,j))
	    {
		std::cout << m[i][j] << " ";
	    }
	    else
	    {
		std::cout << 0 << " ";
	    }
	}
	std::cout << std::endl;
    }
}

//recursive function for marking component in graph. Depth first algorithm.
template<class M>
void mark_components(M &graph, int vertex, int k,int numV,Dune::CpGrid grid)
{
    //We assume that unmarked vertexes has value -1.
    //If the vertex is marked, function does nothing.
    if (graph[vertex][vertex] != -1)
	return;

    //Mark vertex to belong to current component k.
    graph[vertex][vertex]=k;
  
    int numFaces = grid.numCellFaces(vertex);
    int j;
    //loop over possible edges.
    for (j=0;j<numFaces;++j)
    {
	int faceId = grid.cellFace(vertex,j);

	//get cells that faceId borders.
	//Note that one of the cells is cell vertex.
	int faceCell0 = grid.faceCell(faceId,0);
	int faceCell1 = grid.faceCell(faceId,1);
	int otherVertex = faceCell0==vertex ?  faceCell1 : faceCell0;

	if(graph.exists(vertex,otherVertex))
	{
	    //mark vertex at other end of edge.
	    mark_components(graph,otherVertex,k,numV,grid);
	}
    }
}

//function that finds number of components in a graph.
template<class M>
int find_components(M &graph, int numV,Dune::CpGrid grid)
{
    int vertex;
    int numComponents = 0;

    //loop over possible vertices
    for (vertex=0;vertex<numV;++vertex)
    {
	//Check if vertex is part of the graph
	if(graph.exists(vertex,vertex))
	{
	    //Check if vertex is already marked as part of a component. 
	    //Unmarked vertices has value -1.
	    if (graph[vertex][vertex]==-1)
	    {
		//Add a component and start to mark it.
		numComponents++;
		mark_components(graph,vertex,numComponents,numV,grid);
	    }
	}
    }
    return numComponents;
}

//function that finds the number of components of each part of the decomposition.
template<class V>
Dune::BlockVector<Dune::FieldVector<int,1>> find_partition_components(V vector,Dune::CpGrid grid,int mpiSize)
{

    //define data structure to store graphs.
    typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<int,1,1>> MatrixType;
  
    Dune::BlockVector<Dune::FieldVector<int,1>> x(grid.numCells());

    int communicationTable[mpiSize*mpiSize];
    for (int i=0;i<mpiSize;++i)
	for(int j=0;j<mpiSize;++j)
	    communicationTable[i*mpiSize+j]=0;
    int prevNumCom=0;
    int maxComponents = 0;
    int sumComponents = 0;
    
    //loop over all ranks
    for (int i = 0;i<mpiSize;++i)
    {
	//define graph
	Dune::MatrixIndexSet op = getPartitionAdjecency(vector,grid,i,mpiSize,communicationTable);
      
	MatrixType partitionGraph;
	op.exportIdx(partitionGraph);
	partitionGraph = -1;
      
	//find number vertices belonging to current rank
	int localNumCells=0;

	//std::cout<< "Start depth first search" << std::endl;
	//find number of components of part belonging to current rank.
	int numComponents = find_components(partitionGraph, grid.numCells(),grid);

	sumComponents += numComponents;
	maxComponents = numComponents>maxComponents ? numComponents:maxComponents;
	
	int componentNum = 0;
	for (int j=0;j<grid.numCells();++j)
	{
	    if (partitionGraph.exists(j,j))
		x[j] = prevNumCom+ partitionGraph[j][j];
	  
	    if(partitionGraph.getrowsize(j)>0)
	    {
		localNumCells++;
	    }
	}
     
	std::cout << "Rank " << i<< " decomposition has " << numComponents << " components and " << localNumCells<<" values."<<std::endl;

	prevNumCom += numComponents;
    }
  
    double meanComponents = ((double)sumComponents)/((double)mpiSize);
    std::cout << "MPI size: "<< mpiSize<<" Sum Components: " << sumComponents << " Max Components: " << maxComponents << " Mean Components: "<<meanComponents  <<std::endl;
    return x;
}

//recursive function for marking component in graph. Depth first algorithm.
template<class M>
void mark_components(M &graph, int vertex, int k,int numV,Dune::CpGrid grid,Dune::cpgrid::WellConnections wells)
{
    //We assume that unmarked vertexes has value -1.
    //If the vertex is marked, function does nothing.
    if (graph[vertex][vertex] != -1)
	return;

    //Mark vertex to belong to current component k.
    graph[vertex][vertex]=k;
    
    int numFaces = grid.numCellFaces(vertex);
    int j;
    //loop over possible edges.
    for (j=0;j<numFaces;++j)
    {
	int faceId = grid.cellFace(vertex,j);

	//get cells that faceId borders.
	//Note that one of the cells is cell vertex.
	int faceCell0 = grid.faceCell(faceId,0);
	int faceCell1 = grid.faceCell(faceId,1);
	int otherVertex = faceCell0==vertex ?  faceCell1 : faceCell0;

	if(graph.exists(vertex,otherVertex))
	{
	    //mark vertex at other end of edge.
	    mark_components(graph,otherVertex,k,numV,grid,wells);
	}
    }
    for(const auto & well_indicies: wells)
    {
	for(auto well_idx = well_indicies.begin(); well_idx != well_indicies.end();++well_idx)
	{
	    if (graph.exists(vertex,*well_idx))
	    {
		mark_components(graph,*well_idx,k,numV,grid,wells);
	    }
	}
    }
}


//function that finds number of components in a graph.
template<class M>
int find_components(M &graph, int numV,Dune::CpGrid grid,Dune::cpgrid::WellConnections wells)
{
    int vertex;
    int numComponents = 0;

    //loop over possible vertices
    for (vertex=0;vertex<numV;++vertex)
    {
	//Check if vertex is part of the graph
	if(graph.exists(vertex,vertex))
	{
	    //Check if vertex is already marked as part of a component. 
	    //Unmarked vertices has value -1.
	    if (graph[vertex][vertex]==-1)
	    {
		//Add a component and start to mark it.
		numComponents++;
		mark_components(graph,vertex,numComponents,numV,grid,wells);
		//std::cout << numComponents << " " << vertex << std::endl;
	    }
	}
    }
    return numComponents;
}

//function counting number of wells in partition.
template<class M>
std::pair<int,int> find_wells_in_partition(M component,Dune::cpgrid::WellConnections wells)
{
    int numWells=0;
    int numWellCells =0;
    for(const auto & well_indicies: wells)
    {
	bool well_in_part = false;
	for(auto well_idx = well_indicies.begin(); well_idx != well_indicies.end();++well_idx)
	{
	    if (component.exists(*well_idx,*well_idx))
	    {
		well_in_part = true;
		numWellCells++;
	    }
	}
	if(well_in_part)
	    numWells++;
    }

    std::pair<int,int> result = std::make_pair(numWells,numWellCells);
    return result;
}

template <class G,class V>
int edgeCutUniform(V vector, G grid)
{
    int edgeCut =0;

    for (int face=0;face<grid.numFaces();++face)
    {
	//Extract cells connected to face face
	int cellFace0 =grid.faceCell(face,0);
	int cellFace1 =grid.faceCell(face,1);

	//extract coordinates of center of face face
	auto faceCenter = grid.faceCentroid(face);

	//Check if face is on boundary
	if (cellFace0!=-1 && cellFace1!=-1)
	{
	    int rank0 = vector[cellFace0];
	    int rank1 = vector[cellFace1];
	    if (rank0!=rank1)
	    {
		edgeCut++;
	    }
	}
    }
  
    return edgeCut;
}

template <class G,class V>
double edgeCutWeight(V vector, G grid,const double * weight)
{
    double edgeCut =0.0;

    for (int face=0;face<grid.numFaces();++face)
    {
	//Extract cells connected to face face
	int cellFace0 =grid.faceCell(face,0);
	int cellFace1 =grid.faceCell(face,1);

	//extract coordinates of center of face face
	auto faceCenter = grid.faceCentroid(face);

	//Check if face is on boundary
	if (cellFace0!=-1 && cellFace1!=-1)
	{
	    int rank0 = vector[cellFace0];
	    int rank1 = vector[cellFace1];
	    if (rank0!=rank1)
	    {
		edgeCut+= weight[face];
	    }
	}
    }
  
    return edgeCut;
}


//function that finds the number of components of each part of the decomposition.
template<class V>
Dune::BlockVector<Dune::FieldVector<int,1>> find_partition_components(V vector,Dune::CpGrid grid,Dune::cpgrid::WellConnections wells, int mpiSize)
{
    //define data structure to store graphs.
    typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<int,1,1>> MatrixType;
    
    int communicationTable[mpiSize*mpiSize];
    for (int i=0;i<mpiSize;++i)
	for(int j=0;j<mpiSize;++j)
	    communicationTable[i*mpiSize+j]=0;
  
    int cellBoundary[mpiSize*mpiSize];
    for (int i=0;i<mpiSize;++i)
	for(int j=0;j<mpiSize;++j)
	    cellBoundary[i*mpiSize+j]=0;
  
    Dune::BlockVector<Dune::FieldVector<int,1>> x(grid.numCells());

    int prevNumCom=0;
    int maxComponents = 0;
    int sumComponents = 0;
    
    //loop over all ranks
    for (int i = 0;i<mpiSize;++i)
    {
	//define graph
	Dune::MatrixIndexSet op = getPartitionAdjecencyWithWells(vector,grid,wells,i,mpiSize,communicationTable,cellBoundary);

	int numCommNeighbours = 0;
	int numCommFaces      = 0;
	int numBoundaryCells  = 0;
      
	for (int k=0;k<mpiSize;++k)
	{
	    if (communicationTable[mpiSize*i + k]!=0)
	    {
		numCommNeighbours++;
		numCommFaces += communicationTable[mpiSize*i + k];
	    }
	    numBoundaryCells += cellBoundary[mpiSize*i + k];
	}
	
	MatrixType partitionGraph;
	op.exportIdx(partitionGraph);
	partitionGraph = -1;
	
	//find number vertices belonging to current rank
	int localNumCells=0;

	//std::cout<< "Start depth first search" << std::endl;
	//find number of components of part belonging to current rank.
	int numComponents = find_components(partitionGraph, grid.numCells(),grid,wells);

	sumComponents += numComponents;
	maxComponents = numComponents>maxComponents ? numComponents:maxComponents;

	std::pair<int,int> wellInfo =  find_wells_in_partition(partitionGraph,wells);
	
	int componentNum = 0;
	for (int j=0;j<grid.numCells();++j)
	{
	    if (partitionGraph.exists(j,j))
		x[j] = prevNumCom+ partitionGraph[j][j];
	  
	    if(partitionGraph.getrowsize(j)>0)
	    {
		localNumCells++;
	    }
	}
     
	std::cout << "Rank " << i<< " decomposition has " << numComponents << " components and " << localNumCells<<" values. Wells: "<< wellInfo.first<<". Well cells: "<< wellInfo.second<< ". comm Neighbours: " << numCommNeighbours << ". Comm faces: " << numCommFaces << " Boandary cells: "<< numBoundaryCells<<std::endl;

	prevNumCom += numComponents;
    }

    double meanComponents = ((double)sumComponents)/((double)mpiSize);
    std::cout << "MPI size: "<< mpiSize<<" Sum Components: " << sumComponents << " Max Components: " << maxComponents << " Mean Components: "<<meanComponents  <<std::endl;
    return x;
  
}
template<class V>
void read_mat_file(V &v,char* filename,int ts,int cells)
{

    std::ifstream file {filename};
    std::string line;
    for (int i=0;i<5;++i)
    {
	std::getline(file,line);  
    }
    for (int k=0;k<ts;++k)
    {
	Dune::BlockVector<Dune::FieldVector<double,1>> tmp(cells);
	v[k]=tmp;
	v[k]=0;
    }
  
    for (int i=0;i<cells;++i)
    {
	for (int j=0;j<ts;++j)
	{
	    double val;
	    file >> val;
	    v[j][i]=val;
	}
    }
    file.close();
}
