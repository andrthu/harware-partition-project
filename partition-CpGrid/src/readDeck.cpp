/*
  Copyright 2019 Andreas Thune.

  This file is part of the Open Porous Media project (OPM).

  OPM is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  OPM is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OPM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"
#include <array>
#include <vector>
#include <cstdlib>
#include <memory>
#include <cmath>
#include <tuple>
#include <limits>

#define HAVE_ECL_INPUT 1

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/grid/common/datahandleif.hh>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/schwarz.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/istl/paamg/pinfo.hh>

#include <dune/common/function.hh>

#include <mpi.h>

#include <opm/common/utility/parameters/ParameterGroup.hpp>
#include <opm/parser/eclipse/Parser/Parser.hpp>
#include <opm/parser/eclipse/Parser/ParseContext.hpp>
#include <opm/parser/eclipse/Deck/Deck.hpp>
#include <opm/parser/eclipse/EclipseState/EclipseState.hpp>
#include <opm/parser/eclipse/EclipseState/Schedule/Schedule.hpp>
#include <opm/parser/eclipse/Units/Units.hpp>
#include <opm/parser/eclipse/EclipseState/checkDeck.hpp>
#include <opm/parser/eclipse/EclipseState/Grid/Fault.hpp>
#include <opm/parser/eclipse/EclipseState/Grid/FaultFace.hpp>
#include <opm/parser/eclipse/EclipseState/Grid/FaultCollection.hpp>
#include <opm/parser/eclipse/EclipseState/Schedule/Well/Well.hpp>
#include <opm/parser/eclipse/EclipseState/Schedule/TimeMap.hpp>
#include <opm/parser/eclipse/EclipseState/Schedule/Well/WellProductionProperties.hpp>

#include <opm/grid/common/WellConnections.hpp>
#include <opm/grid/utility/OpmParserIncludes.hpp>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/GridHelpers.hpp>

#include <zoltan.h>

#include <opm/material/fluidmatrixinteractions/EclMaterialLawManager.hpp>
#include <opm/core/props/satfunc/RelpermDiagnostics.hpp>
#include <opm/simulators/linalg/ParallelOverlappingILU0.hpp>

#include "io/readDeck.hpp"
#include "io/initClass.hpp"
#include "io/vtkWrite.hpp"
#include "io/comTabInfo.hpp"
#include "io/edgeCutInfo.hpp"
#include "comm/createComm.hpp"
#include "comm/timeComm.hpp"
int main(int argc, char ** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    ReadInit readInit;

    typedef Dune::CpGrid Grid;
    typedef Dune::MPIHelper::MPICommunicator MPICommunicator;
    typedef Dune::CollectiveCommunication<MPICommunicator> CollectiveCommunication;
    typedef Dune::OwnerOverlapCopyCommunication<int,int> Comm;

    typedef Dune::FieldVector<double,1> BlockVec1;
    typedef Dune::FieldVector<double,3> BlockVec3;
    typedef Dune::BlockVector<BlockVec3> Vec;
    
    CollectiveCommunication cc(MPI_COMM_WORLD);
    int rank = cc.rank();
    int mpiSize = cc.size();

    std::string vtkOutPrefix("");
    if (argc > 2)
	readInit.read_file_and_update(argv[2]);
    if (argc > 3) {
	vtkOutPrefix += std::string(argv[3]);
	vtkOutPrefix += std::string("/");
    }
    if(rank == 0)
	readInit.write_param();

    Grid grid;
    ReadProperties readProperties;
    readProperties.init_grid(grid, argv[1], true);
    const double *trans = readProperties.getTransmissibility();
    const auto wells = readProperties.getWells();

    Dune::EdgeWeightMethod edgWgt = Dune::EdgeWeightMethod(std::stoi(readInit.dict[0]));
    int overlapLayers = std::stoi(readInit.dict[1]);
    grid.loadBalance(edgWgt, &wells, trans);//, overlapLayers);

    // Print # Ghost cells, #intrerior cells and # cells
    int numG = printCellInfo(grid, cc);
    
    //create communication pattern
    auto info = getParallelInfo(grid);
    Comm comm(info, cc);
    Vec vec(grid.numCells());
    buildVec(grid, vec);
    
    //Time data transfer
    if (rank == 0) {std::cout << std::endl;}
    multipleMinLoopTimeComm(cc, comm, vec, 10);

    if (rank == 0) {std::cout << std::endl;}
    multipleMinLoopTimeCopyG(cc, vec, numG, 10);
    
    std::vector<int> comTab;
    findAndPrintComTab(grid, comTab, cc);

    writeVTK(grid, rank, mpiSize, edgWgt, vtkOutPrefix);

    std::vector<int> cell_part;
    buildCellPart(grid,cc,cell_part);
    /*
    if (cc.rank() == 0) {
	std::cout <<std::endl;
	for (int i = 0; i < cell_part.size(); ++i)
	    std::cout << cell_part[i] << " ";
	std::cout <<std::endl;
    }
    */
    evaluateEdgeCut(grid, cc, cell_part, trans);
	
    return 0;
}
