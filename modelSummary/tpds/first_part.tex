\title{Detailed modeling of heterogeneous and contention-constrained
  point-to-point MPI communication}
\author{Andreas~Thune,
 			Sven-Arne~Reinemo,  
 			Tor~Skeie,  
 			and Xing~Cai
\IEEEcompsocitemizethanks{
\IEEEcompsocthanksitem \textit{(Corresponding author: Andreas Thune.)}
\IEEEcompsocthanksitem A. Thune, T. Skeie and X. Cai are with Simula Research Laboratory, Kristian Augusts gate 23, Oslo, Norway. E-Mail: andreast@simula.no 
\IEEEcompsocthanksitem SA. Reinemo is with Simula Metropolitan Centre for Digital Engineering, Pilestredet 52, Oslo, Norway.
}

}
\IEEEtitleabstractindextext{
\begin{abstract}
% Heterogeneity and contention are two keywords that characterize
% many-pair, point-to-point MPI communication on a heterogeneous network
% topology, which has a variety of latency and bandwidth values. 
% Here, heterogeneity can
% also apply to the number of neighbors per process and the size per MPI message,
% while contention for the bandwidth can exist among the processes.
\myBlue{The network topology of modern parallel computing systems is
  inherently heterogeneous, with a variety of latency and bandwidth
  values. Moreover, contention
  for the bandwidth can exist on different levels when many
  processes communicate with each other. Many-pair, point-to-point MPI
  communication is thus characterized by heterogeneity and contention,
  even on a cluster of homogeneous multicore CPU nodes.}
To get a detailed understanding of the individual communication cost per
MPI process, we propose a new modeling methodology that
incorporates both heterogeneity and contention. First, we improve the \myBlue{standard}
max-rate model to better quantify the actually achievable bandwidth
depending on the number of MPI processes in competition. Then, we make a further
extension \myBlue{that more detailedly models the bandwidth contention
  when the competing MPI processes have different numbers} of
neighbors, with also non-uniform message sizes. Thereafter, we include
more flexibility by considering interactions \myBlue{between intra-socket and
inter-socket} messaging. % In particular, our main focus
% is on the competition effects between
% intra-socket and inter-socket communication.
Through a series of experiments done on different processor
architectures, we show that the new heterogeneous and
contention-constrained performance models can adequately explain the
individual communication cost associated with each MPI process. 
The largest test of realistic point-to-point MPI communication involves 8,192
processes and in total 2,744,632 simultaneous messages over 64 dual-socket AMD Epyc Rome
compute nodes connected by InfiniBand, for which the overall
prediction accuracy achieved is 8\myBlue{4}\%.
\end{abstract}

\begin{IEEEkeywords}
Point-to-point MPI communication, performance modeling, intra-node communication.
\end{IEEEkeywords}}
\maketitle
\section{Introduction}

\IEEEPARstart{M}{odern} platforms of parallel computing are heterogeneous at least with respect to
the interconnect. Even on a system purely based on
multicore CPUs, the connectivity between the CPU cores has several layers. The 
cores that belong to the same CPU socket can communicate \myBlue{more}
efficiently \myBlue{than those between sockets, because}
% , e.g., through a physically shared cache. The CPU
% sockets on the same compute node of a cluster
%  use a shared memory system, but with non-uniform access speeds. Thus,
 the inter-socket memory bandwidth % , over which the cores can
 % communicate between the sockets,
 is lower than the
 intra-socket counterpart. At the cluster level, between any pair of compute nodes, the
 communication speed is even lower and can depend on the actual
 location of the nodes on the network setup.
All these levels of interconnect heterogeneity will translate  
into vastly different values of the effective latency and bandwidth of
point-to-point MPI communication.

Another complicating factor for many-pair, point-to-point MPI
communication on today's parallel platforms is the potential competition between
different MPI processes. This is because each CPU socket can, if
needed, support a large number of concurrent MPI
processes. Contention arises when multiple pairs of sending-receiving 
processes simultaneously communicate over the same connection.
Such contention may exist, in different magnitudes,
over the entire network. Moreover, the competition situation is often \textit{dynamically} changing;
for instance, an MPI process pair communicating a small message may complete before the
other MPI process pairs, resulting in a reduced level of contention.

This paper aims to detailedly model the per-process
overhead associated with realistic, many-pair, point-to-point MPI
communication. We want to improve the state-of-the-art
quantitative models of point-to-point MPI communication by
\myBlue{a new methodology for modeling the bandwidth
  contention due to}
a large number of %{\em heterogeneous}
MPI processes that compete against each other. Here, %heterogeneity
                                %\myBlue{first and foremost means} that 
\myBlue{communication can exist} on different levels: intra-socket,
inter-socket and inter-node. In addition,  we target the real-world
situation where different MPI processes can have different numbers of
neighbors to exchange data with, while the size of each message is
\myBlue{also} highly non-uniform. One typical example of such a heterogeneous
scenario arises from numerically solving a partial differential
equation (PDE) over an irregular solution domain. The first step of
parallelizing a mesh-based PDE solver is to partition an unstructured
computational mesh, which covers the irregular solution domain, into a desirable
number of subdomains each assigned to an MPI process. The usual
partitioning result is that the number of the nearest
neighbors varies from process to process, and so does the size of each
MPI message.

Specifically, we will propose a new modeling
methodology that quantifies both heterogeneity and contention. At the
same time, we want to inherit a level of simplicity from the fundamental
{\em postal} model~\cite{10.5555/576280,SPAA92_paper} that describes a
single pair of MPI processes, and the successor max-rate
model~\cite{gropp2016modeling}. The elegantly simple postal model
relies on only two parameters to quantify the cost
of point-to-point communication, i.e., a start-up latency $\tau$
and a bandwidth value $BW$. The max-rate model lets $BW$ depend
\myBlue{linearly} on the number of competing MPI processes
\myBlue{while limited from above by a maximum bandwidth value, which is prescribed}
as the third parameter. Our new performance
models are also based on a fair competition among the MPI
processes, but the value of $BW$ will
depend {\em dynamically} on the actual number of competing MPI processes and
how these processes affect each other across \myBlue{two specific levels:
intra-socket and inter-socket.}
The contributions of our work \myBlue{are} as follows:
\begin{itemize}
\item We extend the {\tt osu\_bibw} micro-benchmark of MVAPICH~\cite{mvaphich} to 
  easily tabulate the various $\tau$ and $BW$ values, both
  depend on the connection type and the latter is also a function of the number of
  competing MPI processes. These tabulated values serve as a characterization of the
  communication performance of a heterogeneous interconnect.
\item We improve the accuracy of the max-rate
  model~\cite{gropp2016modeling} \myBlue{for the case of} multiple MPI
  \myBlue{process pairs} concurrently exchanging equal-sized
  messages. Specifically, the tabulated 
  $BW$ values \myBlue{replace an often idealized relationship
    between the achievable bandwidth and the number of competing MPI pairs.}
%an over-simplified roofline model for $BW$.
\item We \myBlue{propose} a ``staircase''  strategy to
  \myBlue{detail the contention between} many
  MPI processes with varying numbers of neighbors and
  message sizes, when competing over a single level of interconnect.
\item We extend the single-level ``staircase'' modeling to mixed-level
  ``staircase'' modeling that also quantifies the interaction between
  \myBlue{two particular communication levels:
intra-socket and  inter-socket.}
\end{itemize}

The remainder of the paper is organized as follows.
Section~\ref{sec:postAndEst} introduces a new bi-directional multi-pair
micro-benchmark, which can be used to pinpoint the achievable
bandwidth as a function of the \myBlue{competing} send-receive
pairs. Section~\ref{sec:model} is devoted to a
new ``staircase'' modeling strategy that can be adopted to handle the
various types of heterogeneity, more
specifically, non-uniform message size, a varied number of
\myBlue{messages} per MPI process, and the interaction between intra-socket
and inter-socket communication. Section~\ref{sec:experiments} 
tests the ``staircase'' strategy and the resulting new models in
realistic cases of many-pair, point-to-point MPI communication. 
%that mixes the intra-socket, inter-socket and inter-node types.
Section~\ref{sec:related_work} places our current work with respect to
the existing work on modeling MPI point-to-point communication,
whereas Section~\ref{sec:conclusion} provides some concluding remarks.
The source code that implements the
mixed-level modeling strategy can be found in the appendix.
