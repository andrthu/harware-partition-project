\section{\myBlue{Example source code}}
\label{sec:appendix}

\myBlue{%
The following C code implements a function that predicts the per-process time usages of many-pair, point-to-point MPI communication, by using the performance model of mixed intra- and inter-socket traffic as described in Section~3.5}
%Section~\ref{sec:3.5}.}

\myBlue{The input arguments are the total number of MPI processes (\texttt{num\_procs}), number of processes per group, two communication matrices (in the \textit{compressed column sparse} format) that contain the source process identity and size of all incoming messages on, respectively, the intra- and inter-socket levels,
 plus two corresponding sets of latency and tabulated bandwidth values. The output argument, \texttt{predictions}, will contain the estimated per-process time usages on return.
}


\lstset{%
    language=C,%
    basicstyle=\tiny\ttfamily,%\color{blue},%
    keywordstyle=\tiny\ttfamily\pmb,%
    morecomment=[l][\color{red}]{//},%
    numbers=left,%
    stepnumber=0}
\begin{lstlisting}
void two_level_predict (int num_procs, int num_procs_pr_group,
                        int *jcols1, int *source_ids1, int *recv_sizes1,
                        int *jcols2, int *source_ids2, int *recv_sizes2,
                        float tau1, float tau2, float *bw_values1, float *bw_values2,
                        float *predictions  /* output */)
{
  int g, i, j, k;
  const int N = num_procs_pr_group;

  // allocation of temporary arrays 
  float *msg_arrive_times1 = (float*)malloc(jcols1[num_procs]*sizeof(float));
  float *msg_arrive_times2 = (float*)malloc(jcols2[num_procs]*sizeof(float));
  float *proc_theta = (float*)malloc(N*sizeof(float));
  float *rest_V = (float*)malloc(N*sizeof(float));

  for (g=0; g<num_procs/N; g++) {  // go through the groups one by one
    int max_msgs_pr_proc = 0, proc_id, num_msgs, recv_v1, recv_v2;

    for (i=0; i<N; i++) {   // preparation work per process
      proc_id = g*N+i; recv_v1 = recv_v2 = 0;

      for (j=jcols1[proc_id]; j<jcols1[proc_id+1]; j++) // summing level-1 messages
        recv_v1 += recv_sizes1[j];
      for (j=jcols2[proc_id]; j<jcols2[proc_id+1]; j++) // summing level-2 messages
        recv_v2 += recv_sizes2[j];

      proc_theta[i] = (recv_v1==0&&recv_v2==0) ? 0 : 1.0*recv_v1/(recv_v1+recv_v2);
      rest_V[i] = recv_v1 + recv_v2;  // total receiving volume per process

      if ((num_msgs=jcols1[proc_id+1]-jcols1[proc_id]) > max_msgs_pr_proc)
        max_msgs_pr_proc = num_msgs;
      if ((num_msgs=jcols2[proc_id+1]-jcols2[proc_id]) > max_msgs_pr_proc)
        max_msgs_pr_proc = num_msgs;
    }

    char *finished = (char*)calloc(N,sizeof(char));  // all initial values 0
    float last_prediction = 0.;

    for (k=0; k<N; k++) {  // step-wise prediction of per-process total receive time
      float min_step = FLT_MAX, step, bw; int index_p = -1;
      for (i=0; i<N; i++) {  // find the process that will finish first in this step
        if (!finished[i]) {
          bw=(proc_theta[i]*bw_values1[N-k]+(1-proc_theta[i])*bw_values2[N-k])/(N-k);
          step = rest_V[i]/bw;
          if (step<min_step) {
            min_step = step; index_p = i;
          }
        }
      }

      for (i=0; i<N; i++)  // go through all the unfinished processes for this step
        if (!finished[i]) {
          bw=(proc_theta[i]*bw_values1[N-k]+(1-proc_theta[i])*bw_values2[N-k])/(N-k);
          rest_V[i] -= min_step*bw;  // update the rest receiving volume
        }

      last_prediction += min_step;
      predictions[g*N+index_p] = last_prediction;
      finished[index_p] = 1;
    }

    int *perm_indices = (int*)malloc(max_msgs_pr_proc*sizeof(int)); // help array

    // dissect per-process total time to pinpoint each message's arrival time
    for (i=0; i<N; i++) {
      proc_id = g*N+i;
      
      // ------- for level-1 incoming messages  -------
      num_msgs = jcols1[proc_id+1]-jcols1[proc_id];
      for (j=0, recv_v1=0; j<num_msgs; j++) {
        recv_v1 += recv_sizes1[jcols1[proc_id]+j]; perm_indices[j] = j;
      }

      // sort the incoming level-1 messages per process according to their sizes
      find_sorting_indices(&(recv_sizes1[jcols1[proc_id]]),perm_indices,0,num_msgs-1);
      
      float last_t = 0., factor = predictions[proc_id]/recv_v1; int last_s = 0;
      for (j=0; j<num_msgs; j++) {  // pinpoint arrival time per level-1 message
        k = jcols1[proc_id]+perm_indices[j];
        last_t += (num_msgs-j)*(recv_sizes1[k]-last_s)*factor;
        msg_arrive_times1[k] = last_t;
        last_s = recv_sizes1[k];
      }
      
      // ------- for level-2 incoming messages  -------
      num_msgs = jcols2[proc_id+1]-jcols2[proc_id];
      for (j=0, recv_v2=0; j<num_msgs; j++) {
        recv_v2 += recv_sizes2[jcols2[proc_id]+j]; perm_indices[j] = j;
      }

      // sort the incoming level-2 messages per process according to their sizes
      find_sorting_indices(&(recv_sizes2[jcols2[proc_id]]),perm_indices,0,num_msgs-1);
      
      last_t = 0.; factor = predictions[proc_id]/recv_v2; last_s = 0;
      for (j=0; j<num_msgs; j++) {  // pinpoint arrival time per level-2 message
        k = jcols2[proc_id]+perm_indices[j];
        last_t += (num_msgs-j)*(recv_sizes2[k]-last_s)*factor;
        msg_arrive_times2[k] = last_t;
        last_s = recv_sizes2[k];
      }
    }

    free (finished); free (perm_indices); // free storage of these two help arrays
  }

  // enlarge per-process predicted time if individual msg delivery time is larger
  for (i=0; i<num_procs; i++)
    for (j=jcols1[i]; j<jcols1[i+1]; j++) {
      k = source_ids1[j];
      if (msg_arrive_times1[j] > predictions[k])
        predictions[k] = msg_arrive_times1[j];
    }
  for (i=0; i<num_procs; i++)
    for (j=jcols2[i]; j<jcols2[i+1]; j++) {
      k = source_ids2[j];
      if (msg_arrive_times2[j] > predictions[k])
        predictions[k] = msg_arrive_times2[j];
    }

  // add the latency
  for (i=0; i<num_procs; i++)
    predictions[i] += (jcols1[i+1]-jcols1[i])*tau1 + (jcols2[i+1]-jcols2[i])*tau2;

  free(msg_arrive_times1);free(msg_arrive_times2);free(rest_V);free(proc_theta);
}
\end{lstlisting}


\myBlue{
The above code consists of two main parts: Pre-calculation of the total volumes of intra- and inter-socket incoming traffic for each process and its individual $\theta_i$ value, according to (12); %(\ref{eq:mixed_bw});  
Estimation of the per-process time usage including a dissection of the delivery time for each message, according to (13)-(17). %(\ref{eq:mix1})-(\ref{eq:mix3}). 
%The latters are used to increase the per-process time when applicable. 
The function \texttt{find\_sorting\_indices} is used to find a permutation array that sorts an un-ordered vector.}
