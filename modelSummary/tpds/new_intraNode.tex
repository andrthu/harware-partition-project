\section{New performance models of many-pair, point-to-point communication} 
\label{sec:model}

\myBlue{Apart from replacing the max-rate formula (\ref{eq:BWmod}) with
tabulated $BW_{\mathrm{MP}}(N)$ values that are determined by the new micro-benchmark of Algorithm~\ref{alg:pp},
another improvement is to more accurately model the bandwidth contention when the competing MPI processes receive largely different volumes. We will thus develop in this section a new modeling strategy that detailedly}
%It, however, still assumes that all the sender-receiver pairs communicate messages of the same size. New performance models are thus needed to 
quantify the {\em per-process} time usage for the cases where the message size is non-uniform \myBlue{and/or} the number of neighbors per process varies. A more general situation is that many MPI messages of various sizes are concurrently communicated over multiple levels of a heterogeneous network. \myBlue{In total three new models of increasing complexity will be introduced.}


\subsection{General many-pair, point-to-point communication}
First let us define the
general situation of many-pair, point-to-point MPI communication in
Algorithm~\ref{alg:cota}. More specifically, MPI process with rank $i$ will simultaneously receive $M_i^{\mathrm{in}}$ messages from $M_i^{\mathrm{in}}$ different neighbors. Each process thus has a list of neighbor identities $\{\textrm{neigh}_j^{\mathrm{in}}\}_{j=0}^{M_i^{\mathrm{in}}-1}$. The neighbors can be of mixed types, i.e., some may reside on the same socket as process $i$, others may reside on a different socket but inside the same compute node, whereas the rest may reside on other compute nodes. \myBlue{Each process is also assumed to send $M_i^{\mathrm{out}}$ outgoing messages to $M_i^{\mathrm{out}}$ neighbors (can be different from the $M_i^{\mathrm{in}}$ ``inward'' neighbors). All the messages can be of varying sizes,  and the sizes of the ``inward'' and ``outward'' messages do not have to match.} 
For this purpose, we have for each process two sets of message sizes $\{\textrm{Rsize}_j\}_{j=0}^{M_i^{\mathrm{in}}-1}$ and $\{\textrm{Ssize}_j\}_{j=0}^{M_i^{\mathrm{out}}-1}$. \myBlue{An example of the resulting sparse \textit{communication matrix} is Fig.~\ref{fig:n16}, which involves} $16$ processes.
It should be remarked that Algorithm~\ref{alg:cota} describes irregular point-to-point MPI communications that frequently arise in scientific computations. One such example can be found in the \texttt{copyOwnerToAll} function of the DUNE software framework~\cite{bastian2008generic2,DUNE_book,DUNE_website}, in connection with parallelizing sparse matrix-vector multiplications.

\begin{figure*}
	\begin{minipage}[t]{0.49\textwidth}
\begin{algorithm}[H]
\caption{Many-pair, point-to-point communication}
		\label{alg:cota}
		\begin{algorithmic}[1]
			\State On each process $i$: $\{\textrm{neigh}_j^{\mathrm{in}}\}_{j=0}^{M_i^{\mathrm{in}}-1}$, $\{\textrm{Rsize}_j\}_{j=0}^{M_i^{\mathrm{in}}-1}$, \myBlue{$\{\textrm{neigh}_j^{\mathrm{out}}\}_{j=0}^{M_i^{\mathrm{out}}-1}$}, $\{\textrm{Ssize}_j\}_{j=0}^{M_i^{\mathrm{out}} -1}$
			\For {$j=0,\ldots,M_i^{\mathrm{out}}-1$}
			\State MPI\_Isend($\textrm{Sbuffer}_j$, $\textrm{Ssize}_j$, $\textrm{neigh}_j^{\mathrm{out}}$, send\_req$_j$)
			\EndFor
			\For {$j=0,\ldots,M_i^{\mathrm{in}}-1$}
			\State MPI\_Irecv($\textrm{Rbuffer}_j$, $\textrm{Rsize}_j$, $\textrm{neigh}_j^{\mathrm{in}}$, recv\_req$_j$)
			\EndFor
			\For {$j=0,\ldots,M_i^{\mathrm{out}}-1$}
			\State MPI\_Wait(send\_req$_j$)
			\EndFor
			\For {$j=0,\ldots,M_i^{\mathrm{in}}-1$}
			\State MPI\_Wait(recv\_req$_j$)
			\EndFor
		\end{algorithmic}
\end{algorithm}
	\end{minipage}
	\begin{minipage}[t]{0.47\textwidth}
		\vspace*{-0.5cm}
		\begin{figure}[H]
				\includegraphics[scale=0.35,trim={10.2cm 2.4cm 9.3cm 1.1cm},clip]{figures/cotaMatHeatUpdate.png}
\caption{An example communication matrix involving $16$ MPI processes. Each black box represents an MPI message, and the number inside the box is the message size.}
		\label{fig:n16}
		\end{figure}
		\centering
	\end{minipage}
\end{figure*}

\subsection{A staircase modeling strategy}
Before developing new performance models of increasing complexity for quantifying the per-process time usage associated with Algorithm~\ref{alg:cota}, \myBlue{we need} to first introduce a ``{staircase}'' modeling strategy. The purpose is to quantitatively describe \myBlue{the} \textit{dynamically} changing scenarios of competition, i.e., the number of competing processes and the available communication bandwidth are both dynamic. The following principles are associated with this modeling strategy:
\begin{enumerate}
\item The time needed by process $i$ to complete all its communication tasks is determined by either how soon it can finish receiving all its $M_i^{\mathrm{in}}$ incoming messages, or how soon all its $M_i^{\mathrm{out}}$ outgoing messages are received at the destinations. The maximum \myBlue{of the two} time usages will apply. The reason for also considering the delivery of the outgoing messages at the destinations is motivated by the most common situation that uses the rendezvous protocol without intermediate buffering, \myBlue{where experiments show that} a sending process cannot complete until all its outgoing messages are delivered.
\item The ``expected''  order of which the $M_i^{\mathrm{in}}$ incoming messages are received by process $i$ is determined by the sizes of these messages. (The actual order is stochastic, thus not modelable.) The smallest incoming message completes first, followed by the second smallest message, and so on. The completion time points for the different incoming messages can be estimated using a {\em staircase} strategy, which will be detailed by \myBlue{formula} (\ref{eq:staircase3}). If the $M_i^{\mathrm{in}}$ incoming messages are of the same size, they are considered to complete simultaneously, due to fairness.
% It is unnecessary (and impossible) to determine the order in which the.
% The $M_i$ incoming messages are thus logically considered to be received with the ``same'' pace, i.e., they complete simultaneously.
% \item The time needed for sending an outgoing message by process $i$ is the same as the time needed by the corresponding destination process to complete receiving all its incoming messages.
\item When multiple receiving processes, each with one or more incoming messages, compete for the same network connection, another staircase principle \myBlue{applies as will be} described in \myBlue{formula} (\ref{eq:nonUni}). The process with the least amount of incoming communication will complete first, letting the remaining processes continue with their remaining communication. This procedure repeats itself until all the processes are completed. At all times, the bandwidth is shared evenly between the still active processes, while the actually available bandwidth can depend on the number of concurrently \myBlue{competing} processes. % The name of ``staircase'' is due to the fact that the actual number of processes that compete for the same connection decreases step-wise.
\end{enumerate}

\subsection{Model for one-level, single-neighbor, non-uniform message size}
\label{sec:one_level_model1}

The staircase modeling strategy will be first applied to the scenario of each MPI process having exactly one incoming message and one outgoing message. The messages sent/received by the different processes \myBlue{can be of various sizes}. For this simple scenario we consider that all the MPI processes communicate on the same level, i.e., either all intra-socket, or all inter-socket, or all inter-node.

To model the per-process time usage, we first group the MPI processes that share the same bandwidth, i.e., the processes that reside on the same socket for the level of intra-socket communication, or the processes on one socket that share the same inter-socket bandwidth, or the processes that reside in one compute node that share the same inter-node bandwidth. We let
$N$ denote the number of processes in a group. The bandwidth under contention is characterized by a set of pre-tabulated values of $BW_{\mathrm{SP}}$, $BW_{\mathrm{MP}}(2)$,$\ldots$, $BW_{\mathrm{MP}}(N)$, as well as a latency constant $\tau$.

Modeling of the per-process time usage will start with sorting the $N$ processes \myBlue{of each group} with respect to the size of the per-process single incoming message: $s_0\le s_1\le\ldots\le s_{N-1}$. Then, the per-process time usage $T_i$ can be estimated as a ``staircase'', more specifically,
\begin{small}
\begin{align}
&t^{\mathrm{recv}}_0=\frac{N\cdot s_0}{BW_{\mathrm{MP}}(N)}, \notag\\
& t^{\mathrm{recv}}_i = t^{\mathrm{recv}}_{i-1} +  \frac{(N-i)\cdot(s_i-s_{i-1})}{BW_{\mathrm{MP}}(N-i)}, 
\;\;i=1,2,\ldots,N-1, 
 \label{eq:nonUni} \\
&T_i = \tau + \max\left(t^{\mathrm{recv}}_i , t^{\mathrm{send}}_i\right). \label{eq:nonUniLat}
\end{align}
\end{small}

The recursive formula in (\ref{eq:nonUni}) accounts for a decreasing degree of bandwidth contention in the form of a staircase, when more and more processes complete. Also, we have used $BW_{\mathrm{MP}}(1) =BW_{\mathrm{SP}}$.
In (\ref{eq:nonUniLat}), $t^{\mathrm{send}}_i$ denotes the time needed for the outgoing message of process $i$ to be delivered on the destination process with rank $k=\mathrm{neigh}_i^{\mathrm{out}}$ (see Algorithm~\ref{alg:cota}), which is considered the same as the receiving time $t^{\mathrm{recv}}_k$ on process $k$.
%The bandwidth term $BW_{on}(N-i)$ can be set using either a max-rate estimate, or more accurate measurements, such as the ones reported in Table \ref{tab:cavium2}. 
\myBlue{We note} that the time usage model in (\ref{eq:nonUni})-(\ref{eq:nonUniLat}) is only valid for single-message per-process communication, but it can be extended to account for multiple message sources and destinations per process. This will be covered in Section~\ref{sec:3.4} below.

\subsubsection*{Verification example}

As the verification experiments for this new model, as well as for verifying the other new performance models later, we have always run a benchmark code that implements Algorithm~\ref{alg:cota}. (The total number of MPI processes, the number of neighbors per process and the sizes of the messages may differ from experiment to experiment.) More specifically, each experiment executes Algorithm~\ref{alg:cota} ten times and the average time usage per iteration is recorded {\em individually} for each MPI process.

We have tested the new model (\ref{eq:nonUni})-(\ref{eq:nonUniLat}) by quantifying the per-process time usage of 6 MPI processes running on one ARM Cavium ThunderX2 processor (all the messages are intra-socket). The 6 processes form three sender-receiver pairs, where each pair exchanges the same amount of data in the two opposite directions. However, the three pairs simultaneously work with three different message sizes: $\frac{s}{2}$, $s$ and $2s$, with $s$ ranging between 2 bytes and 2~MB. The actual time usages for the three pairs are plotted against the model estimates in Fig.~\ref{fig:3size}.
\begin{figure}[h!]

\includegraphics[scale=0.3,trim={1cm 0.9cm 2cm 2.4cm},clip]{figures/T6D0D0.png}
\caption{Single-neighbor, intra-socket messages on an ARM Cavium ThunderX2 CPU. The experiment involves six processes exchanging messages bi-directionally in three pairs. The measured times and model estimates are plotted with dotted and solid lines against the ``middle'' message size $s$. Pairs 0, 1 and 2 exchange, respectively, $2s$, $s$ and $\frac{s}{2}$ bytes of data.}
\label{fig:3size}
\end{figure}

In Fig.~\ref{fig:3size} we can see that the model estimates agree very well with the actual time measurements for most of the message sizes. The model (\ref{eq:nonUni})-(\ref{eq:nonUniLat}) is able to estimate the time usage of each individual process. The two processes of each pair have the same time measurements and estimates, thus we only see three sets of measurement-estimate curves. For each set, we can also observe two clear jumps in the time measurements and estimates, each corresponding to a change in the message protocol, first between the short and eager protocols, then between eager and rendezvous. For example, the eager-rendezvous protocol threshold at 8~KB is clearly visible for the mid-sized pair in the plot.

\subsection{Model for one-level, multi-neighbor, non-uniform message size}
\label{sec:3.4}

%The model (\ref{eq:nonUni})-(\ref{eq:nonUniLat}) assumes that all communication is single-neighbor. 
Now let us consider a more general situation where each process communicates with multiple neighbors. %The resulting new model should therefore be sufficient to handle general communication patterns when all communication happens on the same level, e.g, on-socket.
For this situation,
we modify the recursive formula in (\ref{eq:nonUni})  by replacing the single-message size $s_i$ with a per-process incoming message volume $V_i=\sum_{j=0}^{M_i^{\mathrm{in}}-1} s_{i,j}$, where $M_i^{\mathrm{in}}$ denotes the number of ``inward'' neighbors for process $i$, and $s_{i,j}$ denotes the size per incoming message.
The MPI processes are organized into groups as in the previous model. Again, $N$ denotes the number of processes in a group, and the processes inside the group is now sorted with respect to $V_0\le V_1\le\ldots\le V_{N-1}$.
The time usage estimate in (\ref{eq:nonUni}) for single-neighbor, non-uniform message size communication can now be extended to model the multi-neighbor situation:
\begin{small}
\begin{align}
&t^{\mathrm{recv}}_0=\frac{N\cdot V_0}{BW_{\mathrm{MP}}(N)},\notag\\
&t^{\mathrm{recv}}_i= t^{\mathrm{recv}}_{i-1} +  \frac{(N-i)\cdot(V_i-V_{i-1})}{BW_{\mathrm{MP}}(N-i)}, \;\; i=1,2,\ldots,N-1,
\label{eq:multMes}\\
&T_i = M_i^{\mathrm{in}}\cdot\tau + \max\left(t^{\mathrm{recv}}_i , t^{\mathrm{send}}_{i,0}, t^{\mathrm{send}}_{i,1},\ldots,t^{\mathrm{send}}_{i,M_i^{\mathrm{out}}-1}\right).\label{eq:multMesLat}
\end{align}
\end{small}

In (\ref{eq:multMesLat}), $t^{\mathrm{send}}_{i,j}$ denotes the delivery time needed by each outgoing message, and \myBlue{it equals} the time needed by destination process (with rank $\mathrm{neigh}_{i,j}^{\mathrm{out}}$) to receive this message. Since each process can have multiple incoming messages, we thus need to ``theoretically'' pinpoint the individual time points at which the different messages are received on each destination process. Take for instance process $i$. It is the destination for $M_i^{\mathrm{in}}$ incoming messages, and the total receiving time (without the latency overhead) has been calculated as $t^{\mathrm{recv}}_i$ using (\ref{eq:multMes}). The following recursive formula, which is again based on a staircase principle, will be used to pinpoint the individual time points at which the $M_i^{\mathrm{in}}$ incoming messages are received:
\begin{small}
\begin{align}
&t^{\mathrm{recv}}_{i,0}=\frac{M_i^{\mathrm{in}}\cdot s_{i,0}}{V_i} \cdot t^{\mathrm{recv}}_i,\notag\\
&t^{\mathrm{recv}}_{i,j}=t^{\mathrm{recv}}_{i,j-1}+\frac{(M_i^{\mathrm{in}}-j)\cdot (s_{i,j}-s_{i,j-1})}{V_i}\cdot t^{\mathrm{recv}}_i, \;\; j=1,2,\ldots,M_i^{\mathrm{in}}-1.\label{eq:staircase3}
\end{align}
\end{small}

In the above recursive formula, we have sorted the $M_i^{\mathrm{in}}$ incoming messages for process $i$ with respect to the message sizes $s_{i,0}\le s_{i,1}\le\ldots\le s_{i,M_i^{\mathrm{in}}-1}$, where $\sum_{j=0}^{M_i^{\mathrm{in}}-1} s_{i,j}=V_i$. \myBlue{We} remark that the completion time point for the largest incoming message, which is theoretically expected to finish last, equals the total receiving time needed by process $i$, i.e., $t^{\mathrm{recv}}_{i,M_i^{\mathrm{in}}-1}=t^{\mathrm{recv}}_{i}$.

\begin{table*}[t!]
\caption{Information on the verification examples presented in Fig.~\ref{fig:nab3}. The columns display the communication type, number of MPI processes, total number of messages, maximum/minimum messages per process, total/maximum/minimum(non-zero) per-process communication volume, and the total relative prediction errors for, respectively, the staircase model and the extended max-rate model (\ref{eq:pp_maxrate}).}
\label{tab:expInfo}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\begin{tabular}{ | C{2cm} | c | c | c | c | C{1.8cm} |C{1.8cm} |}
\hline
Communication type & \#procs & \#msgs & max/min & total/max/min volume & Staircase error & Max-rate error \\
\hline
intra-socket& 64 & 192 & 3/3& 16.5MB/0.5MB/0.05MB  & 11.5\% & 26.0\% \\
\hline
inter-socket& 64 & 218 & 7/0 & 19.4MB/1.0MB/0.04MB & 13.0\% &   36.8\%\\
\hline
inter-node& 256 & 3958 & 58/0& 461.7MB/4.3MB/0.3KB  & 12.9\% & 13.0\%  \\
\hline
\end{tabular}
\end{table*}

% \myBlue { In practice, we can combine (\ref{eq:multMes})-(\ref{eq:staircase3}) into a single algorithm. 
% A pseudo-code for modelling intra-node communication is presented in Algorithm \ref{alg:onSocketStair}. 
% For intra-node and single type communication, we can estimate transfer cost separately on each node $\mathcal{N}$, 
% and even do most of the calculations socket-wise.
% On each socket, processes are sorted with respect to total incoming message volume $V_i$, and this order is used to generate 
% the $t^{\mathrm{recv}}_i $ and $t^{\mathrm{recv}}_{i,j}$ estimates of (\ref{eq:multMes}) and (\ref{eq:staircase3}).
% After the computation of these terms on both sockets, the final per-process estimate $T_i$ in (\ref{eq:multMesLat} is evaluated.
% For inter-socket communication, the $T_i$'s are calculated after process estimates from both sockets are completed, 
% since process $j$ in $t^{\mathrm{send}}_{i,j}$ is located on the other socket.}

% \setcounter{algorithm}{2}
% \begin{figure}[ht]
% \begin{algorithm}[H]
% \caption{Single type intra-node communication model}
%  \label{alg:onSocketStair}
% \begin{algorithmic}[1]
% \For {node $\mathcal{N}$}
% \For {$S=0,1$}
% \State \textbf{Sort} $\{V_i\}_{i=0}^{N_S-1}$
% \State $t_0^{\textrm{recv}} = \frac{N_S\cdot V_0}{BW_{\mathrm{MP}}(N_S)}$
% \For {$i=1,...,N_S-1$}
% \State $t^{\mathrm{recv}}_i = t^{\mathrm{recv}}_{i-1} +  \frac{(N_S-i)\cdot(V_i-V_{i-1})}{BW_{\mathrm{MP}}(N_S-i)}$
% \State \textbf{Sort} $\{s_{i,j} \}_{j=0}^{M_i-1}$
% \State $t^{\mathrm{recv}}_{i,0}=\frac{M_i\cdot s_{i,0}}{V_i} \cdot t^{\mathrm{recv}}_i$
% \For{$j=1,..,M_i-1$}
% \State $t^{\mathrm{recv}}_{i,j}=t^{\mathrm{recv}}_{i,j-1}+\frac{(M_i-j)\cdot (s_{i,j}-s_{i,j-1})}{V_i}\cdot t^{\mathrm{recv}}_i$
% \EndFor
% \EndFor
% \EndFor
% \For {$i=0,...,N_{\mathcal{N}}-1$}
% \State $T_i = M_i\cdot \tau + \max\left(t^{\mathrm{recv}}_i , t^{\mathrm{send}}_{i,0},\ldots,t^{\mathrm{send}}_{i,M_i-1}\right)$
% \EndFor
% \EndFor
% \end{algorithmic}
% \end{algorithm}
% \end{figure}

\subsubsection*{Verification examples}

To demonstrate the single-level, multi-neighbor model (\ref{eq:multMes})-(\ref{eq:staircase3}), 
\myGreen{we have conducted three experiments, each consisting exclusively of intra-socket, inter-socket or inter-node traffic. All the results were obtained on a cluster of Epyc Rome CPUs, described in Section~\ref{sec:four_machines}.}
%on three of the machines described in Section~\ref{sec:four_machines}, i.e., ThunderX2, Kunpeng and Epyc Rome, see Table~\ref{tab:cavium2}. 
For \myGreen{the intra-socket experiment}, % and Kunpeng (using one socket), 
a synthetic communication pattern with 64 MPI processes, \myBlue{spread over two sockets,} has been used. As shown in Fig.~\ref{fig:nab3}b, each process sends and receives 3 intra-socket messages of different sizes.
The per-process time estimates are plotted against the actual time measurements in Fig.~\ref{fig:nab3}a.
\myGreen{For comparison, the plot also includes per-process estimates produced by the extended max-rate model (\ref{eq:maxRateUpdate}), marked as purple dots. More specifically, we have used the following variation of (\ref{eq:maxRateUpdate}):
\begin{align}
T_i = M_i\cdot\tau + \max \left( \frac{\min \left(V_{ \mathrm{total}}, NV_i \right)}{ BW_{\max} },\frac{V_{i}}{BW_{\mathrm{SP}}} \right). \label{eq:pp_maxrate}
\end{align} 
That is, the total messaging volume $s_{\mathrm{total}}$ in (\ref{eq:maxRateUpdate}) is replaced by 
$\min \left(V_{ \mathrm{total}}, NV_i \right)$. 
The maximum per-process volume $s_{\max}$ is replaced by per-process volume $V_i$.
Note that (\ref{eq:pp_maxrate}) is the same as (\ref{eq:maxRateUpdate}) for the slowest process.}

%\myBlue {For comparison, the plot also includes per-process estimates produced by the postal model (\ref{eq:post}), i.e., without considering bandwidth contention. In addition, the extended max-rate model (\ref{eq:maxRateUpdate}) has been used to estimate time usage of the \textit{slowest} process per socket, depicted as a horizontal dashed line.}

The \myGreen{inter-socket} experiment uses 64 MPI processes \myBlue{(also spread over two sockets)} that exclusively send and receive inter-socket messages, see Fig.~\ref{fig:nab3}d. The number of neighbors per MPI process and the message sizes arise from partitioning a realistic unstructured computational mesh into 64 pieces, where the maximum number of neighbors per process is 7, and two processes have no neighbors. The message sizes also vary considerably. The actual and estimated per-process times \myBlue{by (\ref{eq:multMes})-(\ref{eq:staircase3})} are displayed in Fig.~\ref{fig:nab3}c.
\myBlue{Max-rate estimates are also included in Fig.~\ref{fig:nab3}c.}

The \myGreen{inter-node} experiment involves 256 MPI processes spread over four nodes, and
the results and inter-node \myBlue{communication pattern} are presented in Fig.~\ref{fig:nab3}e-\ref{fig:nab3}f. 
This example also arises from a realistic case of partitioning an unstructured computational mesh.
Every \myBlue{MPI} process only sends and receives inter-node messages, \myBlue{where} the number of neighbors per process and the message sizes vary considerably. (The maximum number of neighbors per process is 58 whereas the minimum is 0.)
\myBlue{The extended max-rate model (\ref{eq:maxRateUpdate}) has been used to estimate the slowest process time usage per node.
Per-process estimates are not included to prevent cluttering the plot.}
% The postal model estimates are not included to prevent cluttering the plot.}

To quantify the accuracy of our model (\ref{eq:multMes})-(\ref{eq:staircase3}), we calculate a total relative error defined as follows:
\begin{align}
\mbox{Total relative error}=\frac{\sum_{i=0}^{P-1} |T_i^{\mathrm{actual}} - T_i |}{\sum_{i=0}^{P-1} T_i^{\mathrm{actual}}}, \label{eq:err}
\end{align}
where $T_i^{\mathrm{actual}}$ denotes the measured actual time usage on process $i$ and $T_i$ denotes the per-process model estimate. % for Algorithm \ref{alg:cota}. 
Using this error definition, we have calculated the modeling error for the experiments shown in Fig.~\ref{fig:nab3} to be 11.5\% for the intra-socket experiment, 13.0\% for the inter-socket experiment and 12.8\% for the inter-node experiment, see Table~\ref{tab:expInfo}.

\begin{figure*}[ht!]
\centerline{
\begin{subfigure}{.62\textwidth}
\includegraphics[scale=0.36,trim={2.2cm 0.9cm 3.6cm 2.4cm},clip]{figures/betzy4a_MR.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
%\includegraphics[scale=0.26,trim={2cm 0cm 3.3cm 1.8cm},clip]{figures/huaN64On.png}
\includegraphics[scale=0.33,trim={10.4cm 1.1cm 9.3cm 2.1cm},clip]{figures/cavium3nabN64coreHeat.png}
\subcaption{}
\end{subfigure}
}
\centerline{
\begin{subfigure}{.62\textwidth}
\includegraphics[scale=0.36,trim={2.2cm 0.9cm 3.6cm 2.3cm},clip]{figures/betzy4c_MR.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.33,trim={10.4cm 1.1cm 9.3cm 2.1cm},clip]{figures/huaN64OffHeat.png}
\subcaption{}
\end{subfigure}
}
\centerline{
\begin{subfigure}{.62\textwidth}
%\includegraphics[scale=0.36,trim={2.5cm 0.9cm 3.6cm 2.4cm},clip]{figures/betzyJustInterT256N4_MR.png}
\includegraphics[scale=0.36,trim={2.2cm 0.9cm 3.6cm 2.4cm},clip]{figures/fig4e_betzy.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
%\includegraphics[scale=0.26,trim={0cm 0cm 1cm 1.8cm},clip]{figures/betzyJustInterT256N4Heat.png}
\includegraphics[scale=0.33,trim={10.4cm 1.1cm 9.3cm 2.1cm},clip]{figures/betzyJustInterT256N4HeatLines.png}
\subcaption{}
\end{subfigure}
}
%\includegraphics[scale=0.3]{figures/nabs3CoreT64.png}
\caption{Actual time measurement (blue bar), staircase model estimate (green bar) and max-rate estimate (purple dots) for each process on Epyc Rome CPUs. The intra-socket experiment (a) uses a synthetic communication pattern shown in (b), while the inter-socket (c) and inter-node (e) results are obtained using realistic communication patterns shown in (d) and (f), respectively.
}
\label{fig:nab3}
\end{figure*}

\myGreen{As shown in Fig.~\ref{fig:nab3}a-\ref{fig:nab3}c, the extended max-rate model under-estimates the per-process time for the intra- and inter-socket scenarios. 
This is not surprising because it incorrectly models the bandwidth contention.}
%For the same two experiments, the extended max-rate model also under-estimates the slowest process per socket. 
Thus our one-level model (\ref{eq:multMes})-(\ref{eq:staircase3}) gives better accuracy than the extended max-rate models for intra- and inter-socket traffic. For the third experiment, shown in Fig.~\ref{fig:nab3}e, our model has approximately the same accuracy as the extended max-rate model. This result is expected because two competing MPI processes can already saturate the maximum inter-node bandwidth on this system. A more elaborate model of the bandwidth contention thus does not pay off. 
%However, we remark that our model can produce estimates for \textit{all} the processes, which the extended max-rate model can not do.

\subsection{Model for mixing intra- and inter-socket messages}
\label{sec:3.5}

The two new models (\ref{eq:nonUni})-(\ref{eq:nonUniLat})  and (\ref{eq:multMes})-(\ref{eq:staircase3}) can be used to quantify the per-process time usage when all the point-to-point communication happens on one level. However, communications that concurrently take place on different levels can affect each other. We will thus develop a third model to handle a mixture of intra-socket and inter-socket (intra-node) messages. This is important in practice because point-to-point MPI communications on these two levels are particularly subject to the bandwidth contention, which is in essence the contention for the shared memory bandwidth.

The main idea is that the available communication bandwidth, which is to be shared among $N$ competing processes, is neither the pure intra-socket $BW^{\mathrm{on}}_{\mathrm{MP}}(N)$ value nor the pure inter-socket $BW^{\mathrm{off}}_{\mathrm{MP}}(N)$ value. \myBlue{For process $i$, its achievable bandwidth will depend on the ratio between these two traffic types}, more specifically,
\begin{align}
BW_{\mathrm{MP}}^{\mathrm{mix}}(N,\theta_i) &= \frac{\theta_i}{N}\cdot BW^{\mathrm{on}}_{\mathrm{MP}}(N)
+\frac{1-\theta_i}{N}\cdot BW^{\mathrm{off}}_{\mathrm{MP}}(N),\notag\\ \theta_i&=\frac{V^{\mathrm{on}}_i}{V^{\mathrm{on}}_i+V^{\mathrm{off}}_i},
\label{eq:mixed_bw}
\end{align}
\myBlue{where $V^{\mathrm{on}}_i$ and $V^{\mathrm{off}}_i$ denote, respectively, the total intra- and inter-socket incoming volumes on process $i$.}  %That is, in addition to the number of competing processes $N$, $BW_{\mathrm{MP}}(N, \theta)$ also depends on the intra- and inter-socket traffic fractions.

% Another important assumption is that the accessible bandwidth of each process $i$,, i.e., $\frac{1}{N}BW_{\mathrm{MP}}(N,\theta)$, will be further divided between its two incoming traffic types according to the per-process volumes $V_i^{\mathrm{on}}$ and $V_i^{\mathrm{off}}$. For example, the intra-socket traffic of process $i$ can use $$\frac{V_i^{\mathrm{on}}}{(V_i^{\mathrm{on}}+V_i^{\mathrm{off}})}\cdot\frac{1}{N}\cdot BW_{\mathrm{MP}}(N,\theta).$$

To model the per-process receiving time usage $t^{\mathrm{recv}}_i$, \myBlue{when intra- and inter-socket traffic is mixed}, we start with dividing
all the MPI processes into groups based on which socket they reside on. As in the two preceding models, $N$ denotes the number of processes in a group. The challenge now is that we can no longer easily see beforehand the exact order in which the processes will finish receiving their incoming traffic. This is due to a more dynamic competition for the available bandwidth. We will thus adopt a modeling algorithm that is based on yet another staircase strategy consisting of $N$ steps, where in each step we can find out, ``on-the-fly'', which process will be the next one to finish.

\myBlue{
\begin{small}
\begin{align}
%V_i^{\mathrm{recv}}&=0,\;\;t^{\mathrm{recv}}_i=0,\quad \mbox{for }i=0,1,\ldots, N-1.\quad\quad (initialization) \nonumber\\
\mbox{For}& \mbox{ step } k=0,1,\ldots,N-1: \nonumber\\
%& \quad\mbox{Update intra-socket traffic fraction }\notag \\ &\quad\theta=\frac{\displaystyle\sum_{i=0}^{N-1}\left(V_i^{\mathrm{on}}-V_i^{\mathrm{on,recv}}\right)}{\displaystyle\sum_{i=0}^{N-1}\left(V_i^{\mathrm{on}}-V_i^{\mathrm{on,recv}}\right)+\displaystyle\sum_{i=0}^{N-1}\left(V_i^{\mathrm{off}}-V_i^{\mathrm{off,recv}}\right)}.\label{eq:step_fraction}\\
& \mbox{Find an unfinished process }q \mbox{ that gives }\; \notag \\ 
&\;\;\;t_{\mathrm{step}}^k=\min_{i} \frac{V_i^{\mathrm{on}}-V_i^{\mathrm{on,recv}}+V_i^{\mathrm{off}}-V_i^{\mathrm{off,recv}}}{BW_{\mathrm{MP}}^{\mathrm{mix}} (N-k,\theta_i)}; \label{eq:mix1}\\
%&\mbox{Set } \frac{V_q^{\mathrm{on}}-V_q^{\mathrm{on,recv}}+V_q^{\mathrm{off}}-V_q^{\mathrm{off,recv}}}{ BW_{\mathrm{MP}}^{\mathrm{mix}}(N-k,\theta_q)}; \label{eq:model3b}\\
&\mbox{Compute } t^{\mathrm{recv}}_q=\sum_{\ell=0}^k t_{\mathrm{step}}^{\ell};\mbox{(process $q$ finishes after step $k$)}
\label{eq:model3c}\\
&\mbox{Compute } T_q = M_q^{\mathrm{in,on}}\cdot \tau^{\mathrm{on}} + M_q^{\mathrm{in,off}}\cdot \tau^{\mathrm{off}} + \notag \\ & \quad\quad\quad\quad\quad\max\left( t^{\mathrm{recv}}_q, t^{\mathrm{send}}_{q,0}, t^{\mathrm{send}}_{q,1},\ldots, t^{\mathrm{send}}_{q,M_q^{\mathrm{out}}-1}\right);\label{eq:mix1b} \\
& \mbox{For all unfinished processes: }\; \notag \\ 
&\;\;\;V_i^{\mathrm{on,recv}} +=  t_{\mathrm{step}}^k\cdot\theta_i\cdot BW_{\mathrm{MP}}^{\mathrm{mix}} (N-k,\theta_i);
\label{eq:mix2}\\
%& \mbox{For all unfinished processes: }\; \notag \\ 
&\;\;\;V_i^{\mathrm{off,recv}} +=  t_{\mathrm{step}}^k\cdot(1-\theta_i)\cdot BW_{\mathrm{MP}}^{\mathrm{mix}} (N-k,\theta_i).
\label{eq:mix3}
\end{align}
\end{small}
}

In the above model, we have used $V_i^{\mathrm{on,recv}}$ and $V_i^{\mathrm{off,recv}}$ to \myBlue{record} the accumulated volumes of intra- and inter-socket data received \myBlue{so far on process $i$. The values of $V_i^{\mathrm{on,recv}}$ and $V_i^{\mathrm{off,recv}}$ are increased during each step} using (\ref{eq:mix2})-(\ref{eq:mix3}). 
After each step, a process $q$ will finish, as expressed by (\ref{eq:mix1})-(\ref{eq:model3c}). % Its 
% $V_q^{\mathrm{on,recv}}$ and $V_q^{\mathrm{off,recv}}$ values reach exactly $V_q^{\mathrm{on}}$ and $V_q^{\mathrm{off}}$. The above model takes into account that the latest intra-socket traffic fraction $\theta$, see (\ref{eq:step_fraction}), may {\em dynamically} change from step to step.
\myBlue{On each process, its individual ratio $\theta_i$ remains the same throughout the entire process. A source code that implements the above mixed-level model can be found in the appendix.} %Appendix~\ref{sec:appendix}.

% \myBlue {Algorithm \ref{alg:mod} presents a pseudo-code for calculating $t_i^{\mathrm{recv}}$ 
% using the model outlined in (\ref{eq:step_fraction})-(\ref{eq:mix3}). Like in Algorithm \ref{alg:onSocketStair}, 
% we can estimate intra-node process data transfer cost per-node, and do the majority of calculations socket-wise. 
% Other than accounting for a mix of intra- and inter-socket messages, 
% the large difference between Algorithm \ref{alg:onSocketStair} and \ref{alg:mod} is that we no longer sort processes
% with respect to incoming message volume. Although excluded in Algorithm \ref{alg:mod}, 
% the $t^{\mathrm{send}}_{i,j}=t^{\mathrm{recv}}_{j,i}$ terms and final per-process estimates $T_i$ are evaluated in the same way as
% in Algorithm \ref{alg:onSocketStair}.}

% \begin{figure}[ht]
% \begin{algorithm}[H]
% \caption{Intra-node communication model}
%  \label{alg:mod}
% \begin{algorithmic}[1]
% \For{$S=0,1$}
% \State $P_S = \{0,...,N_S-1\}$
% \State Set $\{V_j^{\mathrm{on,r}}\}=\{V_j^{\mathrm{on}}\}, \{V_j^{\mathrm{off,r}}\}=\{V_j^{\mathrm{off}}\}$
% \For{$k=0,...,N_S-1$}
% \State $\theta= \frac{\sum_{j=0}^{N_S-1}V_j^{\mathrm{on,r}}}{\sum_{j=0}^{N_S-1}V_j^{\mathrm{on,r}}+\sum_{j=0}^{N_S-1}V_j^{\mathrm{off,r}}}$
% \For{$j$ in $P_S$}
% \State $t_j = \frac{V_j^{\mathrm{on,r}}+V_j^{\mathrm{off,r}}}{ \frac{1}{N_S-k}\cdot BW(N_S-k,\theta)}$ 
% \EndFor
% \State Find $q$ s.t. $t_q = \min_j t_j$
% \State $t_k^{\mathrm{step}} = t_q$
% \State $t_q^{\mathrm{recv}} = \sum_{\ell=0}^k t_{\ell}^{\mathrm{step}} $
% \For{$j$ in $P_S$}
% \State $V_j^{\mathrm{on,r}} = V_j^{\mathrm{on,r}} - t^{\mathrm{step}}_k\cdot\frac{V^{\mathrm{on}}_i}{V^{\mathrm{on}}_i+V^{\mathrm{off}}_i}\cdot \frac{BW(N_S-k,\theta)}{N_S-k}$
% \State $V_j^{\mathrm{off,r}} = V_j^{\mathrm{off,r}}+ t^{\mathrm{step}}_k\cdot\frac{V^{\mathrm{off}}_i}{V^{\mathrm{on}}_i+V^{\mathrm{off}}_i}\cdot \frac{BW(N_S-k,\theta)}{N_S-k}$
% \EndFor
% \State $P_S= P_S \setminus \{q \}$
% \EndFor
% \EndFor
% \end{algorithmic}
% \end{algorithm}
% \end{figure}

\subsubsection*{Verification examples}
We illustrate the mixed intra/inter-socket model (\ref{eq:mixed_bw})-(\ref{eq:mix3}) with two simple examples, one using the dual-socket ThunderX2 machine and the other using the dual-socket Kunpeng machine (see Table~\ref{tab:cavium2} in Section~\ref{sec:four_machines}). In the first experiment we use in total 48 processes, where 32 processes (16 on each socket) send and receive intra-socket messages and the remaining 16 processes (8 on each socket) send and receive inter-socket messages. 
The results are presented in Fig.~\ref{fig:mixOnOff}a, and the communication pattern is presented in Fig.~\ref{fig:mixOnOff}b. 
The plot in Fig.~\ref{fig:mixOnOff}a includes actual per-process time measurements as blue bars and the staircase model estimate as
green bars.
Notice that the mixed intra/inter-socket model correctly predicts that the off-socket processes complete after the on-socket processes. 
Using the error metric defined in  (\ref{eq:err}), the total relative error is found at 6.6\% for the staircase estimate. 

In the second experiment, we use 64 processes spread over two Kunpeng sockets.
The process connectivity is the same as shown in Fig.~\ref{fig:nab3}b,
but this time we deliberately ``reshuffle'' the process mapping (by the OpenMPI option \texttt{-map-by socket}) to 
 incur a mixture of intra- and inter-socket communication. The per-process message sizes and types are displayed in Fig.~\ref{fig:mixOnOff}d. The actual time measurements and staircase model estimates are displayed in Fig.~\ref{fig:mixOnOff}c. 
The resulting total relative error is 3.6\%. %with the staircase model.

\begin{figure*}[ht!]
\begin{subfigure}{.55\textwidth}
\includegraphics[scale=0.3,trim={2cm 0.9cm 3cm 0cm},clip]{figures/mixOnOffN48_2.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.3,trim={10cm 0.9cm 2cm 0cm},clip]{figures/on-off-heat.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.55\textwidth}
\includegraphics[scale=0.3,trim={2cm 0.9cm 3cm 0cm},clip]{figures/on-off-hua_2.png}
\subcaption{}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[scale=0.3,trim={1cm 0.9cm 1cm 0cm},clip]{figures/on-off-hua-comVol.png}
\subcaption{}
\end{subfigure}
\caption{Plot (a) shows the actual time (blue) and model estimate (green) for an experiment on the dual-socket ThunderX2 machine using 48 processes (evenly spread over two sockets), with the communication pattern shown in  plot (b). Plot (c) shows the actual time and model estimate for an experiment on the dual-socket Kunpeng machine using 64 processes (evenly spread over two sockets). In this experiment each process sends and receives three messages of a varying size. The amount of on- and off-socket traffic each process receives is displayed in plot (d).}
\label{fig:mixOnOff}
\end{figure*}
