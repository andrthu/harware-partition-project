%\section{Background} \label{sec:postAndEst}
\section{Detailing bandwidth contention}
\label{sec:postAndEst}

As mentioned \myBlue{above}, the postal
model~\cite{10.5555/576280,SPAA92_paper} provides an elegant and simple
way of quantifying the time needed to pass a message between
a single pair of MPI send-receive processes. Its formula is as follows:
\begin{align}
T(s) = \tau+\frac{s}{BW_{\mathrm{SP}}}, \label{eq:post}
\end{align}
where the constant parameter $\tau$ denotes the start-up
{latency}, $s$ denotes the size of the MPI message, and
the constant parameter $BW_{\mathrm{SP}}$ denotes the communication bandwidth. The
subscript ``SP'' stands for single-pair and thus highlights
the applicability of the postal model. Experiments (see
e.g.~\cite{HOCKNEY1994389}) have shown that
this two-parameter model can produce estimates of $T(s)$ that agree very well
with the actual single-message time usages, as long as the message size $s$ is within
the regime of the same protocol (i.e., short, eager, or rendezvous). It
also means that each protocol is associated with its specific set of $\tau$
and $BW_{\mathrm{SP}}$ parameters.

The max-rate model~\cite{gropp2016modeling} extends the postal model by considering $N$
competing MPI messages belonging to $N$ send-receive process pairs. \myBlue{If} all
the messages are of the same size
$s$, they \myBlue{will} have the same time usage due to a fair competition
for the bandwidth. The \myBlue{simplest} formula \myBlue{of} the max-rate model is as follows:
\begin{align}
T(N,s) = \tau + \frac{N\cdot s}{BW_{\mathrm{MP}}(N)}. \label{eq:Tmod}
\end{align}

In the above formula, $BW_{\mathrm{MP}}(N)$ is meant to model a shared
bandwidth to be fairly competed among the $N$ messages, and the subscript ``MP''
stands for multi-pair.
The dependency of $BW_{\mathrm{MP}}$ on $N$ is considered by
the max-rate model in its simplest form \myBlue{as follows}:
\begin{align}
BW_{\mathrm{MP}}(N) = \min\left(N\cdot BW_{\mathrm{SP}}, BW_{\max}\right), \label{eq:BWmod}
\end{align}
where $BW_{\max}$ denotes the upper limit of the achievable
communication bandwidth, i.e., a \textit{max rate}. The existence of $BW_{\max}$ illustrates a
saturation effect, which applies to both communication over a network 
connection and communication through shared memory.

\myBlue{
An extended max-rate model was presented in~\cite{bienz2020reducing}
to consider variations in the message size and
number of messages per process. It mainly targets inter-node communication:
%The main goal
%is to account for the imbalance between the processes for inter-node communication.
\begin{align}
T = M\cdot\tau + \max \left( \frac{s_{\mathrm{total}}}{ BW_{\max} },\frac{s_{\max}}{BW_{\mathrm{SP}}} \right). \label{eq:maxRateUpdate}
\end{align}
Here, $M$ is the maximum number of messages per process,
$s_{\mathrm{total}}$ and $s_{\max}$ denote, respectively, the total
messaging volume per node
and the maximum per-process volume. We note that (\ref{eq:maxRateUpdate})
only models the slowest process per node.
}

\myBlue{
While the max-rate model is simple to use, it has several
weaknesses. First, the actual $BW_{\mathrm{MP}}(N)$ value
may not be linearly proportional to $N$ before hitting the upper limit
$BW_{\max}$, especially for intra- and inter-socket traffic.
% This discrepancy is particularly visible for
% communication that is
% enabled through some level of shared memory, as demonstrated later in
% Fig.~\ref{fig:minMax} and Table~\ref{tab:cavium2} in Section~\ref{sec:four_machines}. 
Second, the bandwidth contention may not be accurately modeled when the
processes have largely varying message numbers and/or sizes. Third,
only the slowest process is modeled by (\ref{eq:maxRateUpdate}), not the
earlier finishing processes.
%situations of the competing message having different sizes are not covered. Third, the even 
%more general situations of each MPI process having a varied number of
%messages are not considered. Last but not least, the
%realistic scenario of many MPI processes simultaneously
%communicating over different levels and parts of a heterogeneous network is also
%beyond the max-rate model.
}

In the remainder of this section we will improve the max-rate model
with respect to its first shortcoming. This will be achieved by
extending a micro-benchmark of MVAPICH~\cite{mvaphich}. The other two
shortcomings will be 
addressed in Section~\ref{sec:model}. %, with the help of a new modeling
%strategy and several new models.

\subsection{A new micro-benchmark}
\myBlue{We want} to pinpoint the actual $BW_{\mathrm{MP}}$ values
through measurements obtained by a simple benchmark, instead
\myBlue{of} using the
\myBlue{often idealized formula} (\ref{eq:BWmod}). \myBlue{Specifically}, we will adopt a new
``bi-directional multi-pair'' micro-benchmark, as described in
Algorithm~\ref{alg:pp}. It is a modification of the {\tt osu\_bibw} benchmark
of MVAPICH~\cite{mvaphich}. The new micro-benchmark 
involves $P$ processes to form
$\frac{P}{2}$ sender-receiver pairs, each simultaneously
handling two equal-sized messages of opposite directions. The total
number of competing messages is thus $P$.
To avoid the unwanted 
side-effect of MPI messages being cached, each repetition of a message
is loaded/stored from/to a different location in a pre-allocated long buffer.

\begin{algorithm}[H]
\caption{Bi-directional multi-pair benchmark}
 \label{alg:pp}
\begin{algorithmic}[1]
\State $P$, initial\_size, max\_size, increase\_factor, \myBlue{num\_repeats}
\State $size=\mathrm{initial\_size}$
\While{$size<\mathrm{max\_size}$}
\If{$\textrm{rank}<\frac{P}{2}$}
\For{$i=1,\ldots,\mathrm{\myBlue{num\_repeats}}$}
\State  MPI\_Isend(send\_data\_buffer + $i\cdot size$, $size$, $rank+\frac{P}{2}$)
\State MPI\_Irecv(recv\_data\_buffer + $i\cdot size$, $size$, $rank+\frac{P}{2}$)
\EndFor
\State  MPI\_Waitall()
\Else
\For{$i=1,\ldots,\mathrm{\myBlue{num\_repeats}}$}
\State MPI\_Irecv(recv\_data\_buffer  + $i\cdot size$, $size$, $rank-\frac{P}{2}$)
\State MPI\_Isend(send\_data\_buffer  + $i\cdot size$, $size$, $rank-\frac{P}{2}$)
\EndFor
\State  MPI\_Waitall()
\EndIf
\State $size=\textrm{increase\_factor}\cdot size$
\EndWhile
\end{algorithmic}
\end{algorithm}

\subsection{Example: Measuring $BW_{\mathrm{MP}}(N)$ on four
%  dual-socket CPU 
\myBlue{machines}}
\label{sec:four_machines}

\myBlue{We have run} the bi-directional multi-pair micro-benchmark
(Algorithm~\ref{alg:pp}) on four machines of dual-socket
CPUs. The specific CPU types are: (1) ARM Cavium 32-core ThunderX2
CN9980; (2) ARM 64-core Kunpeng~920-6426; (3) Intel Xeon 26-core
Gold-6230; and (4) AMD 64-core Epyc Rome-7742.
%
OpenMPI v4.0.5 \myBlue{was} used as the MPI installation. 
\myBlue{Compilation used} GCC v10.1.0 with the \texttt{-03
  -march=armv8-a} options on the ThunderX2 and Kunpeng machines, and
GCC v10.2.0 with the \texttt{-03} option on the Intel
Xeon and Epyc Rome machines.
%({\color{red} Need some details of the compilers used}.)
%
On each machine, we measure the time usage
for a series of message sizes in the regime of the rendezvous
protocol.\footnote{Time measurements of messages of size in
  the regime of the short or eager protocol will produce another set of $\tau$ and
  $BW_{\mathrm{MP}}(N)$ values.}
% These are however less relevant for
%  modeling real-world cases that mostly communicate messages of size
%  in the regime of the rendezvous protocol.
This is repeated for some chosen numbers of MPI processes.
For each chosen value $N$,  the series of time measurements (for
the different message sizes) undergo
a linear regression to recover the values of $\tau$ and
$BW_{\mathrm{MP}}(N)$ that can be used in the max-rate model
(\ref{eq:Tmod}) or later in our new models to be introduced in
Section~\ref{sec:model}. On a given machine and for a specific
communication level, the recovered $\tau$ values \myBlue{are very
  similar for} the different choices of $N$. So we
have consistently used the $\tau$ value associated with $N=2$ in
Table~\ref{tab:cavium2} and later experiments.
\myGreen{We remark that the models and experiments to be presented in
  this paper target message sizes in the regime of the rendezvous
  protocol, which is most relevant for real-world cases. Our modeling
  approach remains the same for the other protocols.}

\begin{table*}[t!]
\caption{Values of $\tau$ (in $\mu$s) and $BW_{\mathrm{MP}}(N)$ (in GB/s), obtained from a
  linear regression of the time measurements
 of the bi-directional multi-pair micro-benchmark (Algorithm~\ref{alg:pp}) on four
  dual-socket CPU machines. The tabulated $BW_{\mathrm{MP}}(N)$ values
will improve the accuracy of the max-rate model
(\ref{eq:Tmod})-(\ref{eq:BWmod}) for intra- and inter-socket communication.}
\label{tab:cavium2}
\centerline{
\begin{tabular}{| c | c |c| c | c | c | c | c | c |  }
\hline
CPU type&  Level & $\tau$&{$BW_{\mathrm{SP}}$} &{$BW_{\mathrm{MP}}(2)$} &
                                                     {$BW_{\mathrm{MP}}(4)$} & {$BW_{\mathrm{MP}}(8)$} & {$BW_{\mathrm{MP}}(16)$} & {$BW_{\max}$} \\
\hline
ThunderX2 &intra-socket &2.3& 7.5&14.6 &25.5 &32.5 &45.0 &54.0 \\
 CN9980 &inter-socket &4.4&6.5 &13.7 &17.8 &18.3 &19.7 &22.7 \\
\hline
 Kunpeng & intra-socket  & 3.8 &5.0&10.5 &14.8 &17.0 &19.7 &  53.7\\
 920-6426&inter-socket & 5.1 &4.6 &9.1 &10.4 &13.3 &18.6 &  49.5\\
\hline
Intel Xeon &intra-socket &1.6& 10.0&15.9 &23.8 &31.7 &42.7 &42.7 \\
 Gold 6230&inter-socket & 2.7&8.1& 15.6&22.5 &27.5 & 29.4& 29.4 \\
\hline
AMD Epyc &intra-socket & 1.7&10.2 & 16.8& 17.6& 19.2 & 23.4&51.0 \\
 7742 Rome&inter-socket & 2.9&5.3 & 8.7&11.1 &12.2 &13.0 &30.3 \\
\hline
\end{tabular}}
\end{table*}

The measurement-determined $BW_{\mathrm{MP}}(N)$ values for all the
four machines are summarized in Table~\ref{tab:cavium2}, where we also
distinguish the \myBlue{scenarios} of intra-socket and inter-socket. The former
means that all sender-receiver pairs are placed on the same
socket, whereas the latter means that each pair is split across
two sockets. In this table, and also in the remainder of the paper, the
value $N$ denotes the {number of active MPI processes
per socket that concurrently receive incoming messages}, i.e., running
the micro-benchmark of Algorithm~\ref{alg:pp} with $P=N$ for the
intra-socket measurements and $P=2N$ for the inter-socket measurements. % That is, when
% measuring the inter-socket bandwidths, $N$ processes are placed on each
% socket.
The reason for this definition of $N$ is because the new
performance models to be introduced in Section~\ref{sec:model} consider a
socket as the ``base unit'' when modeling intra- and inter-socket communications.
The $BW_{\max}$ values in Table~\ref{tab:cavium2} 
are obtained from running the same number of MPI
processes per socket as the CPU cores. The intra-socket
$BW_{\mathrm{SP}}$ value is obtained by running two MPI processes on just
one socket, with only one uni-directional message.

\begin{figure*}[t!]
%left,botom,right,top
\includegraphics[scale=0.48,trim={2.5cm 0cm 3.3cm 1.8cm},clip]{figures/comp-min-max-all-arch.png}
\caption{Comparison between measurement-pinpointed
  $BW_{\mathrm{MP}}(N)$ values (solid curves) and those suggested by the \myBlue{formula} (\ref{eq:BWmod})
  \myBlue{of} the max-rate model (dashed curves). The
  measurements are obtained on four dual-socket CPU machines: ARM
  Cavium ThunderX2 (top-left), ARM Kunpeng (top-right), Intel Xeon-Gold
  (bottom-left) and AMD Epyc Rome (bottom-right).}
\label{fig:minMax}
\end{figure*}

One clear observation from Table~\ref{tab:cavium2} is that the $\tau$
measurements associated with the intra-socket and inter-socket cases confirm that
intra-socket MPI communication is faster than the inter-socket
counterpart. %Thus, heterogeneity with respect to the interconnect level cannot be ignored. 
%
Another important observation is that the \myBlue{intra- and inter-socket}
$BW_{\mathrm{MP}}(N)$ values do not follow the formula
(\ref{eq:BWmod}) \myBlue{of} the max-rate model, clearly
demonstrated in Fig.~\ref{fig:minMax}. % Therefore, in the remainder
% of this paper, we will use measurement-pinpointed
% $BW_{\mathrm{MP}}(N)$ values such as those in
% Table~\ref{tab:cavium2}.
These tabulated $BW_{\mathrm{MP}}(N)$ values will be
heavily used in our new models \myBlue{of Section~\ref{sec:model}} that are capable of handling
varying numbers of incoming/outgoing messages
per MPI process, non-uniform size per message, and the interaction
between \myBlue{intra- and inter-socket traffic}.
% communication on the different levels of a heterogeneous interconnect.
% The reason for us to focus here,
% as well as in Section~\ref{sec:model}, only on intra-socket and inter-socket
% communication is because the $BW_{\mathrm{MP}}(N)$ values for the
% inter-compute-node scenario match with the \myBlue{max-rate model's formula} (\ref{eq:BWmod}) quite well.
