\documentclass[10pt]{article}
\usepackage{a4}
\usepackage{moreverb,relsize,url}

\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage[font=small,labelfont=sf]{caption}

\addtolength{\textwidth}{3.2cm}
\addtolength{\oddsidemargin}{-1.cm}
\addtolength{\evensidemargin}{-1.cm}
\addtolength{\textheight}{2.8cm}
\addtolength{\topmargin}{-2.6cm}

\begin{document}

\title{Responses to the comments made by the reviewers}
\author{A.~Thune, S.-A.~Reinemo, T.~Skeie, X.~Cai}
%\date{}
\maketitle

We sincerely thank both reviewers for their detailed and insightful comments. The manuscript has been extensively revised to incorporate the suggestions from the reviewers. (The new and revised texts are marked in the blue color.) Our responses to the specific comments and suggestions are inserted in the text below.

\section*{Comments from Reviewer 1}

% \textit{%
%  The paper introduces a set of performance models for MPI point-to-point communication that captures realistic communication schemes. In particular, the models target modern architectures with combinations of intra-socket, inter-socket, and inter-node communication, and flexible communication patterns with varying numbers of communication targets and varying message sizes per process. Validation experiments compare the model output with benchmark measurements on a variety of hardware architectures, and show that the models produce good predictions with less than 18\% prediction error.
% }

% \textit{%
%  The "staircase modeling" strategy is an interesting and novel approach for modeling MPI communication, with promising results in the given validation examples. It certainly addresses an important need to improve communication models for modern NUMA HPC architectures and applications with dynamic communication patterns. The paper is generally well-written: even though the models are quite complex they are well-presented in the text. The validation experiments use a wide range of different architectures.
% }

\textit{%
 While the modeling approach is very interesting and well-presented, I think the paper is lacking in some aspects:
}

\textit{%
* The validation experiments compare the models against benchmark measurements, but there is no comparison against existing/simpler communication models. Is the additional complexity of the staircase models really necessary? A comparison against the baseline max-rate model would be very helpful to determine how much of an improvement over the state of the art the presented models really are.
}

\vspace{.2cm}\underline{Response}: We have now added, in the three time measurement plots of Fig.~4 (in Section 3.4), comparisons with the predictions that are produced by the postal model and the extended max-rate model. The latter is based on using the latest extension of the max-rate model as described in Bienz et al.``Reducing communication in algebraic multigrid with multi-step node aware communication'', IJHPCA, vol.~34, pp.~547--561, 2020. (We thank Reviewer 2 for bringing this paper to our attention.)

It can be seen from the updated Fig.~4 that our ``staircase'' modeling strategy is, for intra-socket and inter-socket scenarios, more accurate than both the postal and extended max-rate models. More specifically, the predictions of the postal model are, as expected, too optimistic because contention for the bandwidth is not considered at all. Regarding the extended max-rate model (Bienz et al.~2020), its predicted time usage is also too low due to an over-simplified model of the bandwidth contention, when the receiving volumes are very different between competing processes. For the inter-node scenario, our one-level ``staircase'' model (presented in Section 3.4) is still much better than the postal model, but has approximately the same accuracy as the extended max-rate model. The latter is due to the fact that two competing MPI processes can already saturate the maximum inter-node bandwidth. However, we remark that the extended max-rate model can only estimate the slowest process per node, whereas our model produces estimates for all the processes.

%The accuracy of our ``staircase'' model is thus better than that of the max-rate model. More details plus a discussion are provided in the revision.

\vspace{.6cm}

\textit{%
* There is no discussion of use cases for the communication models. What do you ultimately plan to study with them? One or two examples or references would help to motivate this work.
}

\vspace{.2cm}\underline{Response}: One of the main benenfits of using our ``staircase'' modeling approach is the capability of pinpointing per-process time usage, with the help of a more accurate modeling of the bandwidth contention associated with messages of largely varying sizes. Our realistic experiments (using data from real-world mesh partitioning cases) have shown that the actual per-process time usage can vary greatly from process to process, thus the fastest finishing MPI processes may have considerable idle time. Since our performance models can pinpoint the variation in per-process time usage, we plan to incorporate such modeling into future mesh partitioning strategies, with the goal of producing better partitioning results where the fastest finishing processes do not have to wait long for the slowest processes to complete. A new paragraph has been added at the end of the revised manuscript.

%Incorporating the new ``staircase'' performance models into hardware-compatible mesh partitioners will require major work in future, thus beyond the scope of the current paper.

\vspace{.6cm}

\textit{%
* Given the amount of required input data and the need to evaluate them programmatically, the models are almost approaching simulation territory in terms of complexity. It seems to me that they fall somewhere between classic modeling approaches like the LogP family and full-on detailed simulation techniques like discrete event simulation. Can you comment/elaborate on that? Also, what system/framework is used to evaluate the models in practice?
}

\vspace{.2cm}\underline{Response}: In the revised manuscript, we have added an appendix to list the actual C code that implements the mixed two-level communication model of Section 3.5. (The one-level communication model of Section 3.4 is simpler and faster to run.) The purpose is to show that our ``staircase'' modeling strategy is not overly complicated, and can be readily adopted by anyone interested. The needed input data consists of two arrays of tabulated bandwidth values (one for intra-socket and one for inter-socket, with the length of both arrays being the number of processes per socket), two latency constants, and two communication matrices. The latter two are stored in a compressed sparse column format. In other words, the required input data about the communication network is thus rather limited, while the communication matrices are the same as required by other performance modeling approaches. With respect to the computational efficiency, the performance-prediction code is easily parallelized using OpenMP threads, with a typical result that running the performance-prediction code takes much less time on a desktop computer than the actual many-pair, point-to-point MPI communication on a large cluster of SMP nodes.

To summarize, the new ``staircase'' performance models are of rather moderate complexity and do not require substantial input data. The models can efficiently produce the per-process time predictions. No special system/framework is needed to run the performance models.

\vspace{.6cm}


%Overall, this is useful and interesting work. Addressing the points above would significantly improve the paper.

\section*{Comments from Reviewer 2}

\textit{%
% Comments:
The authors present novel contributions to performance modeling as bandwidth does not scale linearly with the number of active processes.  However, there is a lot of wording in the paper that implies postal and max-rate models cannot be used for multiple messages or various sizes, so I suggest that the authors rewrite portions of the paper to be clear about what their contributions are.
}

\vspace{.2cm}\underline{Response}: We thank the reviewer for this comment and
have modified the wording in the revision to clarify that the postal and (extended) max-rate models can be used to handle cases with multiple messages per MPI process and/or messages having various sizes. At the same time, we want to point out that the prediction accuracy achieved by our ``staircase'' model are considerably better than that of the postal and (extended) max-rate models, for the intra- and inter-socket scenarios. %For the inter-node scenario, our``staircase'' model  has very

\vspace{.6cm}


\textit{%
Some larger suggestions :
- "The reason for us to focus here, as well as in Section 3, only on intra-socket and inter-socket communication is because BW values for the inter-compute-node scenario match with the simple roofline model quite well" : this sentence contradicts many statements throughout your paper that state the max-rate model is improved through the use of tabulated BW values.  The max-rate model adds injection bandwidth to the postal model.  This would of course only happen for inter-node messages, which are injected into the network.  The authors continue to add to their models for inter-node communication (section 3 does discuss inter-node), but it is unclear what this model actually is, as this sentence states inter-node communication is modeled with the max-rate model.}

\vspace{.2cm}\underline{Response}: We have modified the sentence in question and several other places in the manuscript. For the inter-node communication, we have used the one-level model of Section 3.4 (multi-neighbor, non-uniform message size). There are differences between our modeling strategy and the (extended) max-rate model for handling the inter-node traffic.
 First, our model predicts the individual time usage per process, whereas the max-rate model focuses only on the slowest process per node (without formula for predicting the time usage of earlier finishing processes).  
Second, if the slowest process per node needs to handle {\em much more} traffic than the other processes, or if {\em many} processes are needed to saturate the inter-node maximum bandwidth, our ``staircase'' model will have better accuracy than the extended max-rate model for predicting the time usage of the slowest process. Otherwise the accuracy for predicting the slowest process will be virtually identical.
%our tabulated BW values can be slightly more accurate than the simple linear relationship between the achievable BW and the number of competing MPI processes (up to the per-node injection rate). Second, our ``staircase'' modeling strategy is unique, thus giving better accuracy, see the updated Fig.~4e for an example . Third, 

\vspace{.6cm}


\textit{%
- "We target the real-world situation where different MPI processes can have different numbers of neighbors to exchange data with, while the size of each message is highly non-uniform" this sentence is right after saying the paper is improving on existing models, and implies the postal and max-rate models cannot be used when processes send multiple messages.  These models are typically used in cases where processes are sending multiple messages (including in the cited papers).
}

\vspace{.2cm}\underline{Response}: We agree that the original sentence is not accurate. This has been modified in the revision.

\vspace{.6cm}


\textit{%
- The max-rate model does originally assume all processes per node send equal data sizes, but this has also been updated in "Reducing Communication in Algebraic Multigrid with Multi-step Node Aware Communication" to account for non-uniform message sizes.  
}

\vspace{.2cm}\underline{Response}: We thank the reviewer for pointing Bienz et al.~2020 to us. The description and discussion about the max-rate model have been modified in several places in the revision. %Nevertheless, we consider our ``staircase'' modeling of the bandwidth contention to be more accurate, confirmed by new performance comparisons added in Figs.~4a \& 4c.

\vspace{.6cm}



\textit{%
- The authors list as contributions 'targets multiple pairs of MPI processes concurrently exchanging equal-sized messages' and 'handle many pairs of MPI processes, with varying numbers of neighbors and different message sizes'.  It is unclear how these are novel.  If there is an additional contribution here over those in existing postal and max-rate models, these contributions should be clarified to state how this paper is contributing over existing models.
}

\vspace{.2cm}\underline{Response}: We have revised the text in several places of the new manuscript to clarify our contributions. In our view, the main contribution of our  work is the``staircase'' modeling methodology targeting the bandwidth contention. For the intra- and inter-socket communication scenarios, our new performance model is considerably more accurate. Please see our response to the first comment of Reviewer 1.

%gives better accuracy than the postal and max-rate models. (The updated plots in Fig.~4 now have comparisons with these two models.) The better accuracy is due to the ``staircase'' modeling of the bandwidth contention situation, which is completely absent from the postal model, whereas in the max-rate model, according to Bienz et al.~2020, the contention among different sized messages is over-simplified (as formula (2) in that paper). 

\vspace{.6cm}


\textit{%
- The authors main contributions are creating models that improve over the max-rate model, but they never compare against the max-rate model.  This should be a straightforward comparison as the max-rate model can be used for various message sizes and split into intra-socket, intra-node, and inter-node like in [15].  It seems the main differences are on-node bandwidth measures.
}

\vspace{.2cm}\underline{Response}: The three time measurement plots in Fig.~4 have been updated by adding comparisons with the extended max-rate model, as well as the postal model. The better accuracy of our ``staircase'' model is confirmed for the intra- and inter-socket scenarios. As stated above, our model has also benefits in the inter-node scenario.


\vspace{.6cm}

\textit{%
Some smaller suggestions :
- Heterogeneous often implies systems that contain heterogeneous nodes (e.g., GPUs or FPGAs).  The paper focuses on SMP nodes, so the word heterogeneous is a bit confusing.
}

\vspace{.2cm}\underline{Response}: We have revised the start of the abstract to clarify that heterogeneity refers, first and foremost, to the network topology, which is also true for clusters of SMP nodes, because there co-exist different levels of bandwidth and latency values. We have also made it clearer in the introduction that the focus of the paper is on modeling the cost of many-pair, point-to-point MPI communication on clusters of SMP nodes, where the competing MPI processes are heterogeneous with respect to the number of neighbors and size of the messages.

\vspace{.6cm}

\textit{%
- Similarly, 'roofline' is often used when talking about roofline models, which are different than the max-rate model.  If there is another way to describe the max-rate model, that would make the paper a bit easier to understand.}

\vspace{.2cm}\underline{Response}: We agree with the reviewer's remark. The term 'roofline' is no longer used in the revised manuscript to avoid confusion.

\vspace{.6cm}

\textit{%
- "the heterogeneity exceeds that the MPI processes can run on..." this sentence is missing a word I think}

\vspace{.2cm}\underline{Response}: We have reformulated this sentence in the revision.

\vspace{.6cm}


\textit{%
- "MPI process with rank i will simultaneously receive M>1 messages from M different neighbors (one message per neighbor) and will at the same time send one message to each of these neighbors."  Why do they have to send to and receive from the same neighbors?  This is often not the case for PDE solvers (described in the introduction).}

\vspace{.2cm}\underline{Response}: Indeed, for each process, the destinations for its outgoing messages do not have to be the same as the sources for its incoming messages. The numbers of the outgoing and incoming messages may also differ. Our modeling strategy does not need any restrictions in this regard. We have modified this part of text in the revision.

\vspace{.6cm}


\textit{%
- "The sizes of the M sending messages can be non-uniform, so do the M receiving messages" there is a typo in this sentence}

\vspace{.2cm}\underline{Response}: This sentence is rephrased in the revision.

\vspace{.6cm}


\textit{%
- "Neither should the sizes of the sending and receiving messages match" implies they cannot match.  Is there a reason for that?  Or is it just that they do not have to match?}

\vspace{.2cm}\underline{Response}: It was indeed meant that they do not have to match. This sentence is rephrased in the revision.

\vspace{.6cm}


\textit{%
- Rendezvous messages "a sending process cannot complete until all its outgoing messages are delivered" isn't exactly correct.  Rendezvous sends cannot complete until the receiving process is ready for the message, but they can complete before that data has all been received.
}

\vspace{.2cm}\underline{Response}: We agree with the reviewer on this remark. The original sentence was more based on our observations with the actual time measurements, than an interpretation of the MPI standard. The sentence is reformulated in the revision.

\vspace{.6cm}

\textit{%
- Principle 2 in your modeling strategy : Why is the expected order based on message size?  Are these only for on-node messages?  Otherwise, I would think relative location may be a better predictor?
}

\vspace{.2cm}\underline{Response}: This principle is used to order multiple incoming messages of the same type for a receiving process. The rationale is based on a ``fairness'' assumption that the larger the message the longer the transfer time will be. This principle applies to on-node and inter-node messages alike.

We remark that our modeling strategy {\em can} be extended to incorporate the topological heterogeneity of inter-node traffic. One possible approach is to group nodes that are tightly connected (e.g., all-to-all connected through a switch), so that two or even more levels are considered when modeling the inter-node communication.

\vspace{.6cm}

\textit{%
- Fig 4: why are intra-socket messages not realistic, like inter-socket and inter-node?  Is there a reason intra-socket messages are 'synthetic'?
}

\vspace{.2cm}\underline{Response}: The choice of the 'synthetic' intra-socket case in Fig.~4a is motivated by our wish to study more closely the performance model, such as the need for waiting for the delivery of outgoing messages. For example, processes 0,1,2,3 communicate with each other, but the receiving amount per process differs. If the message delivery times at the destinations are not considered, processes 0,1,2,3 will be predicted to complete at different times. However, because processes 0,1,2 each sends an equal-sized message to process 3, which has the largest receiving amount among the four, the first three processes are predicted to complete at the same time as process 3. This predicted behavior is confirmed by the actual time measurements.

\vspace{.6cm}

\textit{%
- Is there an explanation for why bandwidth is adjusted when sending both intra-socket and inter-socket?  Is this the case regardless of the size, or only when they are both transported through shared memory?  Is there also contention when intra-socket is transferred through cache and inter-socket through main memory?  If so, do the authors have intuition as to why?
}

\vspace{.2cm}\underline{Response}: When there is a mixture of intra- and inter-socket traffic, we reason that the two traffic types have to compete for the overall bandwidth, thus none will get the full bandwidth. The reason for dynamically adjusting the accessible bandwidth per process is the same as for the single-type communication scenario, i.e., the competition decreases as more and more processes complete. (In the revision, we have simplified the performance model for mixing intra- and inter-socket communication in Section 3.5.)

In our models, we have not explicitly considered the scenario where intra-socket messages may be transferred through cache. The reason is that we are more interested in modeling situations where the message sizes are large and transported using the rendezvous protocol. For such large messages, we consider that message transfers are always through the shared memory system, because the capacity of a shared cache is typically insufficient.

\end{document}
