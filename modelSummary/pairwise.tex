%\section{Background} \label{sec:postAndEst}
\section{Detailing bandwidth contention}
\label{sec:postAndEst}

As mentioned in the previous section, the postal
model~\cite{10.5555/576280,SPAA92_paper} provides an elegant and simple
way of quantifying the time needed to pass a message between
a single pair of MPI send-receive processes. Its formula is as follows:
\begin{align}
T(s) = \tau+\frac{s}{BW_{\mathrm{SP}}}, \label{eq:post}
\end{align}
where the constant parameter $\tau$ denotes the start-up
{latency}, $s$ denotes the size of the MPI message, and
the constant parameter $BW_{\mathrm{SP}}$ denotes the communication bandwidth. The
subscript ``SP'' stands for single-pair and thus highlights
the applicability of the postal model. Experiments (see
e.g.~\cite{HOCKNEY1994389}) have shown that
this two-parameter model can produce estimates of $T(s)$ that agree very well
with the actual single-message time usages, as long as the message size $s$ is within
the regime of the same protocol (i.e., short, eager, or rendezvous). It
also means that each protocol is associated with its specific set of $\tau$
and $BW_{\mathrm{SP}}$ parameters.

The max-rate model~\cite{gropp2016modeling} extends the postal model by considering $N$
competing MPI messages belonging to $N$ send-receive process pairs. Here, all
the messages are of the same size
$s$, they thus have the same time usage due to a fair competition
for the bandwidth. The formula for calculating
the time usage by the max-rate model is as follows:
\begin{align}
T(N,s) = \tau + \frac{N\cdot s}{BW_{\mathrm{MP}}(N)}. \label{eq:Tmod}
\end{align}

In the above formula, $BW_{\mathrm{MP}}(N)$ is meant to model a shared
bandwidth to be fairly competed among the $N$ messages, and the subscript ``MP''
stands for multi-pair.
The dependency of $BW_{\mathrm{MP}}$ on $N$ is considered by
the max-rate model as a ``roofline'' in its simplest form, namely,
\begin{align}
BW_{\mathrm{MP}}(N) = \min\left(N\cdot BW_{\mathrm{SP}}, BW_{\max}\right), \label{eq:BWmod}
\end{align}
where $BW_{\max}$ denotes the upper limit of the achievable
communication bandwidth, i.e., a ``max rate''. The existence of $BW_{\max}$ illustrates a
saturation effect, which applies to both communication over a network 
connection and communication through shared memory.

While the max-rate model is elegant and simple to use, it has several
weaknesses. First, the actually achievable $BW_{\mathrm{MP}}(N)$ value is often
not linearly proportional to $N$ before hitting the upper limit
$BW_{\max}$. This discrepancy is particularly visible for
communication that is
enabled through some level of shared memory, as demonstrated later in
Figure~\ref{fig:minMax} and Table~\ref{tab:cavium2} in Section~\ref{sec:four_machines}. Second, the
situations of the competing message having different sizes are not covered. Third, the even 
more general situations of each MPI process having a varied number of
messages are not considered. Last but not least, the
realistic scenario of many MPI processes simultaneously
communicating over different levels and parts of a heterogeneous network is also
beyond the max-rate model.

In the remainder of this section we will improve the max-rate model
with respect to its first shortcoming. This will be achieved by
extending a micro-benchmark of MVAPICH~\cite{mvaphich}. The other
shortcomings of the max-rate model will be 
addressed in Section~\ref{sec:model}, with the help of a new modeling
strategy and several new models.


% The traditional model for estimating the cost of MPI point-to-point communication, when sending a message of $s$ bytes between two processes is called the postal model. This model is based on a latency parameter $\tau$ and a per-byte data transfer cost parameter $\alpha$:
% \begin{align}
% T = \tau + \alpha s \label{eq:post}
% \end{align}
% Obviously, cost of communicating a massage intra- or inter-node differs, and the latency and transfer cost parameters in the postal model must be adjusted to fit each case. For intra-node communication we must also separate on- and off-socket messages. On some architectures process location relative to the Host Channel Adapter (HCA) card may also impact inter-node performance. 
% %In addition, communication cost depend on which MPI protocol is used to send the data. 
% We therefore introduce a parameter $L$ for the cost $T_{L}$ representing process level $L=\{\textrm{on-socket}, \textrm{off-socket}, \textrm{inter-node},...\}$.
% % and protocol $P=\{short,eager,rendezvous\}$.

% The postal model models a single pair of processes sending a single message. 
% With a simple reformulation the cost model in Eq. \ref{eq:post} can accommodate $N$ processes corresponding to $\frac{N}{2}$ pairs bi-directionally exchanging messages of size $s$.
% %A simple reformulation of the cost model in Eq. \ref{eq:post} allows for $N$ processes corresponding to $\frac{N}{2}$ pairs bi-directionally exchanging messages of total size $s$. 
% One can also use a bandwidth term $BW_{L}(N)$ as the inverse of the data transfer parameter $\alpha$:
% \begin{align}
% T_{L}(N,s) = \tau_{L} + \frac{Ns}{BW_{L}(N)}. \label{eq:Tmod}
% \end{align}
% Note that the latency $\tau_{L}$ and bandwidth $BW_{L}(N)$ depend on the method used to send the messages. The MPI implementation we consider, uses three methods for sending data: The \textit{short}, \textit{eager} and \textit{rendezvous} protocols. The short protocol is used for small messages that can be packed into the meta-data envelope. Messages that exceed the envelope size, are sent using the eager or rendezvous protocols. The eager protocol assumes available buffer size on the receiving side, and immediately sends the data. In the rendezvous protocol data transfer happens after receiver has allocated buffer space. The eager protocol is used for relatievley small messages, while the rendezvous protocol is used for larger messages at the cost of higher latency.
% \\
% \\
% As already mentioned communication cost differs when messages are sent on- or off-socket and intra- or inter-node. In addition to message protocol and message type, the bandwidth also depends on the number of processes involved in sending messages. The max-rate model \cite{gropp2016modeling} can be used to estimate the bandwidth for increasing number of processes. In this framework two parameters, single process bandwidth $b_{L}$ and maximal bandwidth $B_{L}$, are used to model the total bandwidth:
% %The reason for this, is that on many architectures, multiple processes are required to fully saturate the memory access bandwidth. We can model the bandwidth $BW_{L,P}(N)$ with single pair bandwidth $b_{L,P}$ and the maximal bandwidth $B_{L,P}$:
% \begin{align}
% BW_{L}(N) = min(Nb_{L}, B_{L}). \label{eq:BWmod}
% \end{align}
% The bandwidth expression in Eq. \ref{eq:BWmod} inserted into the multi-pair postal model in Eq. \ref{eq:Tmod} is the max-rate model. There will be an upper bound $B_{L}$ on both intra- and inter-node communication. For the intra-node case this bound is reached when the memory-bandwidth is saturated. We identify several drawbacks with this approach. First, the roofline bandwidth estimate assumes that the bandwidth increases linearly with the number of message receiving processes. We have observed that is often not the case, and therefore prefer to set $BW_{L}(N) $ using measurements. Second, the max-rate model does not properly model cases with mixed intra- and inter-node messages.

\subsection{A new micro-benchmark}
Our goal here is to pinpoint the actual $BW_{\mathrm{MP}}$ values
through measurements obtained by a simple benchmark, instead using the
over-simplified ``roofline'' (\ref{eq:BWmod}). For this purpose, we will adopt a new
``bi-directional multi-pair'' micro-benchmark, as described in
Algorithm~\ref{alg:pp}. It is a modification of the {\tt osu\_bibw} benchmark
of MVAPICH~\cite{mvaphich}. More specifically, the new micro-benchmark 
involves $P$ processes to form
$\frac{P}{2}$ sender-receiver pairs, where each pair simultaneously
handles two equal-sized messages of opposite directions. The total
number of competing messages is thus $P$, the same as the total number of processes.
To avoid the unwanted 
side-effect of MPI messages being cached, each repetition of a message
is loaded/stored from/to different locations of a pre-allocated long buffer.

% To estimate the parameters of the point-to-point MPI communication cost model, we use a ping-pong benchmark. This benchmark is described in Algorithm \ref{alg:pp}, and is a modification of the OSU bibw benchmark. The benchmark is a bi-directional ping-pong test, where messages are sent in pairs between $N$ processes. For each pair we send multiple repeating messages. The ping-pong benchmark differs from the osu\_bibw in that it allows more than a single pair. Because we are interested in intra-node point-to-point communication, we send and receive data from different locations of a larger vector. This is done to avoid cache effects, and without this feature in the ping-pong test, the bandwidth results would be artificially large. 
% \\
\begin{algorithm}[ht!] 
\KwData{$P$, initial\_size, max\_size, increase\_factor, num\_repetitions}
$size=\mathrm{initial\_size}$\;
\While{$size<\mathrm{max\_size}$}{ 
\eIf{$\textrm{rank}<\frac{P}{2}$}{
\For{$i=1,\ldots,\mathrm{num\_repetitions}$}{
MPI\_Isend(send\_data\_buffer + $i\cdot size$, $size$, $rank+\frac{P}{2}$)\;
MPI\_Irecv(recv\_data\_buffer + $i\cdot size$, $size$, $rank+\frac{P}{2}$)\;
}
MPI\_Waitall()\;
}{
\For{$i=1,\ldots,\mathrm{num\_repetitions}$}{
MPI\_Irecv(recv\_data\_buffer  + $i\cdot size$, $size$, $rank-\frac{P}{2}$)\;
MPI\_Isend(send\_data\_buffer  + $i\cdot size$, $size$, $rank-\frac{P}{2}$)\;
}
MPI\_Waitall()\;
}
$size=\textrm{increase\_factor}\cdot size$\;
}
\caption{Bi-directional multi-pair benchmark \label{alg:pp}}
\end{algorithm}

%%We want to use the ping-pong benchmark in Algorithm \ref{alg:pp} to estimate the message data transfer bandwidth on different hardware levels (on/off-socket and inter-node). To do this we need a way to map processes to specific cores on a node. When using OpenMPI4.0.5 process mapping is enabled through the \texttt{--rankfile} command line argument, which allows users to submit a file mapping ranks to selected CPU slots. With the rankfile feature the ping-pong benchmark can be used to estimate cost model parameters for on/off-socket and inter-node communication.

\subsection{Example: Measuring $BW_{\mathrm{MP}}(N)$ on four
  dual-socket CPUs}
\label{sec:four_machines}

To demonstrate the use of the bi-directional multi-pair micro-benchmark
(Algorithm~\ref{alg:pp}), we have run it on four machines of dual-socket
CPUs. The four specific CPU types are: (1) ARM Cavium 32-core ThunderX2 CN9980
CPU; (2) ARM 64-core Kunpeng~920-6426 CPU; (3) Intel Xeon
26-core Gold-6230 CPU; and (4) AMD 64-core Epyc Rome-7742 CPU.
%
OpenMPI v4.0.5 has been used as the MPI installation. 
The benchmark was compiled using GCC v10.1.0 with the \texttt{-03
  -march=armv8-a} options on the ThunderX2 and Kunpeng machines, and
GCC v10.2.0 with the \texttt{-03} option on the Intel
Xeon and Epyc Rome machines.
%({\color{red} Need some details of the compilers used}.)
%
On each machine, we measure the time usage
for a series of message sizes in the regime of the rendezvous
protocol.\footnote{Time measurements of messages of size in
  the regime of the short or eager protocol will produce another set of $\tau$ and
  $BW_{\mathrm{MP}}(N)$ values. These are however less relevant for
  modeling real-world cases that mostly communicate messages of size
  in the regime of the rendezvous protocol.}
This is repeated for some chosen numbers of MPI processes.
For each chosen value $N$,  the series of time measurements (for
the different message sizes) undergo
a linear regression to recover the values of $\tau$ and
$BW_{\mathrm{MP}}(N)$ that can be used in the max-rate model
(\ref{eq:Tmod}) or later in our new models to be introduced in
Section~\ref{sec:model}. On a given machine and for a specific
communication level, the recovered $\tau$ values that are
associated with the different choices of $N$ are very similar. So we
have consistently used the $\tau$ value associated with $N=2$ in
Table~\ref{tab:cavium2} and later experiments.

\begin{table}[t!]
\caption{Values of $\tau$ (in $\mu$s) and $BW_{\mathrm{MP}}(N)$ (in GB/s), obtained from a
  linear regression of the time measurements
 of the bi-directional multi-pair micro-benchmark (Algorithm~\ref{alg:pp}) on four
  dual-socket CPU machines. The tabulated $BW_{\mathrm{MP}}(N)$ values
will improve the accuracy of the max-rate model (\ref{eq:Tmod})-(\ref{eq:BWmod}).}
\label{tab:cavium2}
\centerline{
\begin{tabular}{| c | c |c| c | c | c | c | c | c |  }
\hline
CPU type&  Level & $\tau$&{$BW_{\mathrm{SP}}$} &{$BW_{\mathrm{MP}}(2)$} &
                                                     {$BW_{\mathrm{MP}}(4)$} & {$BW_{\mathrm{MP}}(8)$} & {$BW_{\mathrm{MP}}(16)$} & {$BW_{\max}$} \\
\hline
ThunderX2 &intra-socket &2.3& 7.5&14.6 &25.5 &32.5 &45.0 &54.0 \\
 CN9980 &inter-socket &4.4&6.5 &13.7 &17.8 &18.3 &19.7 &22.7 \\
\hline
 Kunpeng & intra-socket  & 3.8 &5.0&10.5 &14.8 &17.0 &19.7 &  53.7\\
 920-6426&inter-socket & 5.1 &4.6 &9.1 &10.4 &13.3 &18.6 &  49.5\\
\hline
Intel Xeon &intra-socket &1.6& 10.0&15.9 &23.8 &31.7 &42.7 &42.7 \\
 Gold 6230&inter-socket & 2.7&8.1& 15.6&22.5 &27.5 & 29.4& 29.4 \\
\hline
AMD Epyc &intra-socket & 1.7&10.2 & 16.8& 17.6& 19.2 & 23.4&51.0 \\
 7742 Rome&inter-socket & 2.9&5.3 & 8.7&11.1 &12.2 &13.0 &30.3 \\
\hline
\end{tabular}}
\end{table}

The measurement-determined $BW_{\mathrm{MP}}(N)$ values for all the
four machines are summarized in Table~\ref{tab:cavium2}, where we also
distinguish the situations of ``intra-socket'' and ``inter-socket''. The former
means that all sender-receiver pairs are placed on the same
socket, whereas the latter means that each pair is split across
two sockets. In this table, and also in the remainder of the paper, the
value $N$ denotes the {number of active MPI processes
per socket that concurrently receive incoming messages}, i.e., running
the micro-benchmark of Algoritm~\ref{alg:pp} with $P=N$ for the
``intra-socket'' measurements and $P=2N$ for the ``inter-socket'' measurements. % That is, when
% measuring the inter-socket bandwidths, $N$ processes are placed on each
% socket.
The reason for this definition of $N$ is because the new
performance models to be introduced in Section~\ref{sec:model} consider a
socket as the ``base unit'' when modeling intra- and inter-socket communications.
The $BW_{\max}$ values in Table~\ref{tab:cavium2} 
are obtained from running the same number of MPI
processes per socket as the CPU cores. The ``intra-socket''
$BW_{\mathrm{SP}}$ value is obtained by running two MPI processes on just
one socket, with only one uni-directional message.

\begin{figure}[t!]
%left,botom,right,top
\includegraphics[scale=0.48,trim={2.5cm 0cm 3.3cm 1.8cm},clip]{figures/comp-min-max-all-arch.png}
\caption{Comparison between measurement-pinpointed
  $BW_{\mathrm{MP}}(N)$ values (solid curves) and those suggested by the ``roofline'' (\ref{eq:BWmod})
  that is adopted by the max-rate model (dashed curves). The
  measurements are obtained on four dual-socket CPU machines: ARM
  Cavium ThunderX2 (top-left), ARM Kunpeng (top-right), Intel Xeon-Gold
  (bottom-left) and AMD Epyc Rome (bottom-right).}
\label{fig:minMax}
\end{figure}

One clear observation from Table~\ref{tab:cavium2} is that the $\tau$
measurements associated with the ``intra-socket'' and ``inter-socket'' cases confirm that
intra-socket MPI communication is faster than the inter-socket
counterpart. Thus, heterogeneity with respect to the
interconnect level cannot be ignored. 
%
Another important observation from Table~\ref{tab:cavium2} is that the
$BW_{\mathrm{MP}}(N)$ values do not follow the simple ``roofline''
(\ref{eq:BWmod}) that is
adopted by the max-rate model. This deviation is more clearly
demonstrated in Figure~\ref{fig:minMax}. Therefore, in the remainder
of this paper, we will use measurement-pinpointed
$BW_{\mathrm{MP}}(N)$ values such as those in
Table~\ref{tab:cavium2}. These tabulated $BW_{\mathrm{MP}}(N)$ values will be
heavily used in our new models that are capabile of handling
heterogeneity in the form of a varying number of incoming/outgoing messages
per MPI process, non-uniform size per MPI message, and the interaction
between communication on the different levels of a heterogeneous interconnect.
The reason for us to focus here,
as well as in Section~\ref{sec:model}, only on intra-socket and inter-socket
communication is because the $BW_{\mathrm{MP}}(N)$ values for the
inter-compute-node scenario match with the simple ``roofline'' (\ref{eq:BWmod}) quite well.

% \subsection*{Estimating bandwidth and latency on dual-socket CPUs}
% We want to illustrate the use of the ping-pong benchmark for estimating intra- and inter-node message transfer bandwidth and latency on different computing platforms. By varying individual message sizes from 1B to 4MB we are able to estimate message transfer parameters for both the eager and rendezvous protocols. Because Algorithm \ref{alg:pp} allows multiple pairs, we can investigate how the message bandwidth changes as we increase the number of active processes. In our experiments we consider four computing platforms: ARM Cavium ThunderX2 CN9980, Kunpeng920-6428, Intel Xeon-Gold 6230 and AMD Epyc 7742 Rome. Some hardware characteristics of these CPUs are presented in Table \ref{tab:cpu}.
% \\
% \begin{table}[ht!]
% \caption{General computing platform information.}
% \label{tab:cpu}
% \begin{tabular}{| c | c | c | c |}
% \hline
% CPU Name & Cores per socket& NUMA domains & Interconnect\\
% \hline
% ARM Cavium ThunderX2 CN9980 & 32 &2&  ConnectX-6 (200 Gb/s)\\
% \hline
% Kunpeng920-6428 & 64 & 4 &ConnectX-6 (200 Gb/s)\\
% \hline
% Intel Xeon-Gold 6230 & 26 & 2 & ConnectX-5 (100 Gb/s)\\
% \hline
% AMD Epyc 7742 Rome & 64 & 2 &ConnectX-6 (200 Gb/s)\\
% \hline
% \end{tabular}
% \end{table}
% \\
% Notice that Kunpeng has two NUMA domains per socket. Still, on all architectures, intra-node point-to-point messages are either of on-socket or off-socket type. 
% All CPUs, except for the Xeon-Gold, are equipped with Mellanox ConnectX-6 infiniband capable of achieving 200 Gb/s, or 25 GB/s, node-to-node bandwidth. On Xeon-Gold the ConnectX-5 infiniband interconnect the inter-node bandwidth is 100 100 Gb/s, or 12.5 GB/s.
% For Cavium, Kunpeng and Xeon-Gold we have access to only four nodes each, and in our experiments on these CPUs we will always stay on a single switch.
% \\
% \\ 
% With the ping-pong benchmark in Algorithm \ref{alg:pp} we can estimate the bandwidth for the eager and rendezvous protocols for increasing number of processes. Because we also are interested in message latency, we use the measurements from Algorithm \ref{alg:pp} to estimate bandwidth and latency with linear regression. The rendezvous protocol parameters are generated by measurements where message sizes are larger than the rendezvous threshold, which we set to 8K bytes. Likewise we estimate eager protocol bandwidth and latency with point-to-point messages smaller than 8K bytes. We present the latencies and single pair bandwidth on the four computing platforms in Table \ref{tab:lat}.
% \\
% \begin{table}[ht!]
% \caption{Single pair message latencies and bandwidths on the Cavium, Kunpeng Xeon-Gold and Rome CPUs. Latency is measured in micro seconds (us) and bandwidth in GB/s. On-socket, off-socket and inter-node values are presented for the short, eager and rendezvous protocols. }
% \label{tab:lat}
% \begin{tabular}{| c | c | c  c  c | c  c  c |}
% %\hline
% %CPU & Message type & Short & Eager & Rendezvous  \\
% %\hline
% %Cavium & On-socket & 0.3 &0.45 & 2.3 \\
% %& Off-socket& 0.5 &0.85 &4.4 \\
% %& Inter-node & 0.25/0.46 & 0.48/0.89 & 1.4/2.6\\
% %\hline
% %Kunpeng & On-socket & 0.21 & 0.38& 3.8 \\
% %& Off-socket& 0.35 & 0.78 & 5.1 \\
% %& Inter-node & 0.25 & 0.29 & 2.2 \\
% %\hline
% %Xeon-Gold & On-socket & 0.13& 0.28 &1.6 \\
% %& Off-socket& 0.16& 0.58 & 2.7 \\
% %& Inter-node &0.4 & 0.52 &2.9 \\
% %\hline
% %Epyc Rome & On-socket & 0.13 &0.27 &1.7 \\
% %& Off-socket& 0.25 &0.67 & 2.9\\
% %& Inter-node & 0.44 &  0.54 & 1.9 \\
% \hline
% CPU & Protocol & $\tau_{on}$ &$\tau_{off}$ & $\tau_{inter}$  & $BW_{on}$&  $BW_{off}$&  $BW_{inter}$\\
% \hline
% Cavium & Short & 0.3& 0.5& 0.25/0.46 &-- & --&-- \\
% & Eager & 0.45  & 0.85& 0.59/0.89 & 13.6& 5.5& 20.4/10.3\\
% & Rendezvous & 2.3& 4.4& 1.2/1.8 & 14.6& 13.7& 21.7/13.8\\
% \hline
% Kunpeng & Short & 0.21& 0.35& 0.25 &-- & --& --\\
% & Eager &  0.38 & 0.78& 0.54 & 9.5& 5.4& 14.5\\
% & Rendezvous & 3.8 & 5.1& 2.1 & 10.5& 9.1& 40.3\\
% \hline
% Xeon-Gold & Short & 0.13& 0.25&0.44 &-- &-- &-- \\
% & Eager & 0.28  & 0.58 & 0.44 & 12.5& 3.5& 11.1\\
% & Rendezvous &1.6 &2.7 & 1.5 & 15.9 & 15.6& 12.0\\
% \hline
% Epyc Rome& Short & 0.13 & 0.25 & 0.44 &-- & --& --\\
% & Eager &  0.27 & 0.67 & 0.54 & 9.5& 4.5& 14.8\\
% & Rendezvous & 1.7 & 2.9 & 1.5 &16.8 &8.7 & 24.8\\
% \hline
% \end{tabular}
% \end{table}
% \\
% In Table \ref{tab:lat} latency and bandwidth results for different message protocols are displayed. Because the short protocol is used for very small messages that fit into the meta data envelope, we do not report bandwidth estimates for this protocol. 
% Notice that the inter-node Cavium results show two results. The origin of these duplicate results is that inter-node performance is sensitive to process placement in relation to the HCA card location on Cavium. If processes are located on the "wrong" socket we observe a significant loss of bandwidth and increase in latency. 
% The inter-node bandwidth on Kunpeng is also of interest, as it achieves 40 GB/s in the rendezvous protocol, despite the interconnect only allowing for 25 GB/s data transfer rate. The reason for the measurements exceeding this limit, is that the Kunpeng CPUs have two connections, one on each socket, and is able to use both when communicating between nodes.
% On the Xeon-Gold and Epyc Rome CPUs, the rendezvous inter-node bandwidth is close to the hardware limit of 12.5 and 25 GB/s. On all computing platforms, increasing the number of active processes communicating inter-node, does not change the observed bandwidth. In contrast, intra-node bandwidth, both on- and off-socket, increase with the number of active processes.
% \\
% \\
% Intra-node bandwidth estimates from the four computing platforms are presented in Table \ref{tab:cavium2}. We report the results against the number of active receiver processes per socket $N$, and we include bandwidth estimates for the $N=1$ case, when there is only one process that receives a message. Because each socket (or NUMA domain in the Kunpeng case) is equipped with memory channels independent of each other, it is sufficient to report the achievable on-socket message transfer bandwidth on a single socket.
% %Measuring off-socket bandwidth obviously require active processes on both sockets. However, we choose to report both on- and off-socket bandwidth for the number of message receiving processes per socket, and not per node. 
% %The on- and off-socket bandwidth results are presented in Table \ref{tab:cavium2}. Note that the eager protocol is used when sending messages lower than the rendezvous threshold, and consequently the rendezvous protocol is used when messages are larger than or equal to the rendezvous threshold. 
% \\
% \begin{table}[ht!]
% \caption{Measured eager and rendezvous intra-node MPI message transfer bandwidth (GB/s) on ARM Cavium ThunderX2 CN9980,  Kunpeng920-6428, Intel Xeon-Gold and AMD Epyc 7742 CPUs against increasing number of message receiving processes per socket. Both on-socket and off-socket bandwidth is reported.}
% \label{tab:cavium2}
% \begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c |  }
% \hline
% &  & $N$ & 1 & 2 & 4 & 8 & 16 & 26 & 32 & 64 \\
% \hline
% Cavium & Eager & on-socket & 6.8 & 13.6& 24.8& 50.5 &67.9 & &95.1 & \\
%  & Eager & off-socket& 2.8 &5.5 &10.7 & 21.2 & 31.2 & &50.1 &\\
%  & Rendezvous &on-socket & 7.5&14.6 &25.5 &32.5 &45 & &54.0 & \\
%  & Rendezvous &off-socket &6.5 &13.7 &17.8 &18.3 &19.7 & &22.7 & \\
% \hline
%  Kunpeng & Eager & on-socket & 4.5&9.5 &12.8 &23.4 &41.4 & & 60 & \\
%  & Eager & off-socket & 2.5&5.4 &9.6 &17.2 &30 & &36 & \\
%  & Rendezvous &on-socket & 5&10.5 &14.8 &17.0 &19.7 & & 26.9 & \\
%  & Rendezvous &off-socket &4.6 &9.1 &10.4 &13.3 &18.6 & & 24.5 & \\
% \hline
% Xeon-Gold & Eager & on-socket & 6&12.5 &25.5 &47.9 &92.4 &145 & & \\
%  & Eager & off-socket & 1.7& 3.5&6.6 & 10.9& 11.9& 13.5& & \\
%  & Rendezvous &on-socket & 10&15.9 &23.8 &31.7 &42.7 &42.7 & & \\
%  & Rendezvous &off-socket & 8.1& 15.6&22.5 &27.5 & 29.4& 29.4& & \\
% \hline
% Epyc Rome & Eager & on-socket & 8.6& 9.5& 22.8& 43.4& 51& & 80& 166 \\
%  & Eager & off-socket & 2.6 & 4.5&9.6 &17.7 &30 & &34.4 & \\
%  & Rendezvous &on-socket & 10.2 & 16.8& 17.6& 19.2 & 23.4& &34.8 &51 \\
%  & Rendezvous &off-socket & 5.3 & 8.7&11.1 &12.2 &13 & &17.5 &30.3 \\
% \hline
% \end{tabular}
% \end{table}
% \noindent
% \\
% In the intra-node bandwidth results in Table \ref{tab:cavium2} we observe that there is a significant difference in on- and off-socket bandwidth on all CPUs for both the eager and rendezvous protocols. 
% The reported on-socket bandwidth is achieved on a single socket (or NUMA domain on Kunpeng). If all processes on both sockets were active, the aggregated bandwidth would be doubled.
% We can use the results in Table \ref{tab:cavium2} to set the $b_{L}$ and $B_{L}$ parameters in the max-rate model in Eq. \ref{eq:BWmod}. Take for example the ARM Cavium CPU. The $N=1$ and $N=32$ columns can be used to set $b_{L}$ and $B_{L}$ for on/off-socket and eager/rendezvous protocols. These parameters are presented in Table \ref{tab:cavium1} for the Cavium, Kunpeng, Xeon-Gold and Epyc Rome CPUs. 
% %In addition to the max-rate parameters, Table \ref{tab:cavium1} displays message transfer latency for on-socket and off-socket messages when using either the eager or rendezvous protocols on the four architectures. The latency was attained using the same benchmark that produced the bandwidth results in \ref{tab:cavium2}.
% \begin{table}[h!]
% \caption{Bandwidth parameters (GB/s) for the transfer cost model in Eq. \ref{eq:Tmod}. Results displayed for eager and rendezvous protocols on the Cavium, Kunpeng Xeon-Gold and Rome CPUs. }
% \label{tab:cavium1}
% %\begin{tabular}{| l | c c | c c c c | c c | c c c c | }
% \begin{tabular}{| l | c c c c |  c c c c | }
% \hline
% & \multicolumn{4}{l |}{ Eager } & \multicolumn{4}{l |}{ Rendezvous } \\
% \hline
% %CPU& $\tau_{on}$ & $\tau_{off}$ & $b_{on}$ & $B_{on}$ & $b_{off}$ & $B_{off}$ & $\tau_{on}$ & $\tau_{off}$ & $b_{on}$ & $B_{on}$  & $b_{off}$ & $B_{off}$\\
% CPU & $b_{on}$ & $B_{on}$ & $b_{off}$ & $B_{off}$ &  $b_{on}$ & $B_{on}$  & $b_{off}$ & $B_{off}$\\
% \hline
% Cavium      & 6.8 & 95.1& 2.8 & 50.1 &7.5 &54 & 6.5& 22.7\\
% Kunpeng   & 4.5 &60 &2.5 &35  &5 &26.9 &4.6 & 24.5\\
% Xeon-Gold & 6 &145 &1.7 &13.5 &10 &42.7 &8.1 & 29.4\\
% Rome        &  8.6 &166 & 2.6 & & 10.2 &51 &5.3 &30.3\\
% \hline
% %\begin{tabular}{| l | r  r  r | r  r  r |}
% %\hline
% %& \multicolumn{3}{l |}{ On-socket } & \multicolumn{3}{l |}{ Off-socket } \\
% %\hline
% %Protocol & $\tau_{on,P}$ & $b_{on,P}$ & $B_{on,P}$& $\tau_{off,P}$ & $b_{off,P}$ & $B_{off,P}$\\
% %\hline 
% %Eager & 0.45us& 13.6GB/s &95.1GB/s&	0.85us & 2.8 GB/s & 50.1 GB/s \\
% %\hline
% %Rendezvous & 2.3us & 7.5GB/s& 54.0 GB/s& 4.4us& 6.5 GB/s & 22.7 GB/s \\
% %\hline
% \end{tabular}
% \end{table}
% \\
% \\
% Because we have intra-node bandwidth results on several computing platforms for increasing number of active receiver processes, we can compare the measurements with the max-rate roofline estimate. 
% In Figure \ref{fig:minMax} we present max-rate bandwidth estimates using the expression in Eq. \ref{eq:BWmod} and the parameters in Table \ref{tab:cavium1} in four plots together with intra-node bandwidth results from Table \ref{tab:cavium2}. In the plots, we display on- and off-socket results for the rendezvous protocol from all four computing platforms.
% We observe that the max-rate roofline approach overestimates both the on- and off-socket bandwidth on all architectures. 
% \\
% \begin{figure}[h!]
% %left,botom,right,top
% \includegraphics[scale=0.4,trim={2.5cm 0cm 3.3cm 1.8cm},clip]{figures/comp-min-max-all-arch.png}
% \caption{Comparison of intra-node measured and max-rate point-to-point communication bandwidth. The plots display rendezvous on-socket (red) and off-socket bandwidth (blue). The stippled line represent the max-rate bandwidth, while the solid line is the measured bandwidth. Each subplot display results for the four different CPUs: Arm Cavium (top-left), Kunpeng (top-right), Intel Xeon-Gold (bottom-left) and AMD Rome (bottom-right).}
% \label{fig:minMax}
% \end{figure}
% \\
% The measured message transfer bandwidth and latency reported in Table
% \ref{tab:cavium2} and Table \ref{tab:cavium1} were attained with the
% ping-pong benchmark in Algorithm \ref{alg:pp}. In this benchmark
% messages are sent pairwise and bidirectionally. The bandwidth and
% latency results attained on the different computing platforms can
% however be used as a basis for models estimating message transfer cost
% for cases with more complicated communication patterns. We will
% present and investigate such modelling in the following sections.
