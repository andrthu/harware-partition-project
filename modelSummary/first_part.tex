\title{Detailed modeling of heterogeneous and contention-constrained
  point-to-point MPI communication}
\author{Andreas Thune \and Sven-Arne Reinemo \and Tor Skeie \and Xing Cai}

\maketitle

\begin{abstract}
Heterogeneity and contention are two keywords that characterize
many-pair, point-to-point MPI communication on a heterogeneous network
topology, which has a variety of latency and bandwidth values. 
Here, heterogeneity can
also apply to the number of neighbors per process and the size per MPI message,
while contention for the bandwidth can exist among the processes.
To get a detailed understanding of the individual communication cost per
MPI process, we propose a new modeling methodology that
incorporates both heterogeneity and contention. First, we improve the existing
max-rate model to better quantify the actually achievable bandwidth
depending on the number of MPI processes in competition. Then, we make a further
extension by allowing each MPI process to have a varied number of
neighbors, with also non-uniform message sizes. Thereafter, we include
more flexibility by considering interactions between intra-socket,
inter-socket and inter-node messaging. In particular, our main focus
is on the competition effects between
intra-socket and inter-socket communication.
Through a series of experiments done on different processor
architectures, we show that the new heterogeneous and
contention-constrained performance models can adequately explain the
individual communication cost associated with each MPI process. 
The largest test of realistic point-to-point MPI communication involves 8,192
processes and in total 2,744,632 simultaneous messages over 64 dual-socket AMD Epyc Rome
compute nodes connected by InfiniBand, for which the overall
prediction accuracy achieved is 83\%.
\end{abstract}


\section{Introduction}

Modern platforms of parallel computing are heterogeneous at least with respect to
the interconnect. Even on a system purely based on
multicore CPUs, the connectivity between the CPU cores has several layers. The 
cores that belong to the same CPU socket can communicate very
efficiently, e.g., through a physically shared cache. The CPU
sockets on the same compute node of a cluster
 use a shared memory system, but with non-uniform access speeds. Thus, the
 inter-socket memory bandwidth, over which the cores can
 communicate between the sockets, is lower than the
 intra-socket counterpart. At the cluster level, between any pair of compute nodes, the
 communication speed is even lower and can depend on the actual
 location of the nodes on the network setup.
All these levels of interconnect heterogeneity will translate  
into vastly different values of the effective latency and bandwidth of
point-to-point MPI communication.

Another complicating factor for many-pair, point-to-point MPI
communication on today's parallel platforms is the potential competition between
different MPI processes. This is because each CPU socket can, if
needed, support a large number of concurrent MPI
processes. Contention arises when multiple pairs of sending-receiving 
processes simultaneously communicate over the same connection.
Such contention may exist, in different magnitudes,
over the entire network. Moreover, the competition situation is often dynamically changing;
for instance, an MPI process pair communicating a small message may complete before the
other MPI process pairs, resulting in a reduced level of contention.

This paper aims to detailedly model the per-process
overhead associated with realistic many-pair, point-to-point MPI communication on
modern computing platforms. We want to improve the state-of-the-art
quantitative models of point-to-point MPI communication by
considering a large number of {\em heterogeneous}
MPI processes that compete against each other. Here, the heterogeneity
exceeds that the MPI processes can run on processor cores that are heterogeneously
connected: intra-socket, inter-socket and inter-node. In addition, 
we target the real-world situation where different MPI processes can
have different numbers of neighbors to exchange data with, while the size of
each message is highly non-uniform. One typical example of such a heterogeneous
scenario arises from numerically solving a partial differential
equation (PDE) over an irregular solution domain. The first step of
parallelizing a mesh-based PDE solver is to partition an unstructured
computational mesh, which covers the irregular solution domain, into a desirable
number of subdomains each assigned to an MPI process. The usual
partitioning result is that the number of the nearest
neighbors varies from process to process, and so does the size of each
MPI message.

Specifically, we will propose a new modeling
methodology that quantifies both heterogeneity and contention. At the
same time, we want to inherit a level of simplicity from the fundamental
{\em postal} model~\cite{10.5555/576280,SPAA92_paper} that describes a
single pair of MPI processes, and the successor max-rate
model~\cite{gropp2016modeling}. The elegantly simple postal model
relies on only two parameters to quantify the cost
of point-to-point communication, i.e., a start-up latency $\tau$
and a bandwidth value $BW$. The max-rate model lets $BW$ depend on the number of competing
MPI processes in the shape of a ``roofline'', where the top of the
roofline is introduced as the third parameter. Our new performance
models are also based on a fair competition among the MPI
processes, but the value of $BW$ will
depend {\em dynamically} on the actual number of competing MPI processes and
how these processes affect each other across the different levels:
intra-socket, inter-socket and inter-node.
The contributions of our work can be summarized as follows:
\begin{itemize}
\item We extend the {\tt osu\_bibw} micro-benchmark of MVAPICH~\cite{mvaphich} to 
  easily tabulate the various $\tau$ and $BW$ values, which both
  depend on the connection type and the latter is also a function of the number of
  competing MPI processes. These tabulated values serve as a characterization of the
  communication performance of a heterogeneous interconnect.
\item We improve the accuracy of the max-rate
  model~\cite{gropp2016modeling} that targets multiple pairs of MPI
  processes concurrently exchanging equal-sized
  messages. Specifically, we use the tabulated 
  $BW$ values instead of an over-simplified roofline model for $BW$.
\item We introduce a ``staircase''  modeling strategy to handle
  many pairs of MPI processes, with varying numbers of neighbors and
  different message sizes, that compete over a single level of interconnect.
\item We extend the single-level ``staircase'' modeling to mixed-level
  ``staircase'' modeling that also quantifies the interaction between
  the different communication levels:
intra-socket, inter-socket and inter-node.
\end{itemize}

% Of particular value is this modeling methodology to legacy MPI code
% which is not easily reprogrammed to adopt a mixed MPI+X style. The
% emphasis of our work is on quantifying the intra-node point-to-point
% MPI communication. Modern CPU architectures can easily give rise to
% more than 100 CPU cores per compute node (in a dual-socket setting). 

The remainder of the paper is organized as follows.
Section~\ref{sec:postAndEst} will introduce a new bi-directional multi-pair
micro-benchmark, which can be used to pinpoint the achievable
bandwidth as a function of the computing send-receive
pairs. Section~\ref{sec:model} will be devoted to a
new ``staircase'' modeling strategy that can be adopted to handle the
various types of heterogeneity, more
specifically, non-uniform message size, varied number of
sender/receivers per MPI process, and the interaction between intra-socket
and inter-socket communication. Section~\ref{sec:experiments} will
test the ``staircase'' strategy and the resulting new models in
realistic cases of many-pair, point-to-point MPI communication that
mixes the intra-socket, inter-socket and inter-node types.
Section~\ref{sec:related_work} will place our current work with respect to
the existing work on modeling MPI point-to-point communication,
whereas Section~\ref{sec:conclusion} will provide some concluding remarks.

%%% The following text has been moved to another file  

% \section{Related work}
% The defacto standard for modelling the performance of parallell processing systems was for an long time the postal model as proposed in ~\cite{10.5555/576280,SPAA92_paper}. The postal model has the benefit of being very simple, but its performance estimates are only accurate for fixed size, single messages on homogenous systems. The limitation of single messages was addressed in the more recent ``max-rate'' model~\cite{gropp2016modeling} which extends the postal model to support N concurrent messages from N send-receive process pairs, but the lack of support for variable size messages, heterogeous latencys and an over-simplified bandwidth roofline remain a weakness of in the max-rate model.

% Another body of work on communication models is the LogP family of models. This orginated in the LogP model first proposed by Culler et al. in ~\cite{SPAA92_paper}. The LogP model adds communication overhead as a third parameter to better estimemate the cost of sending and receiving messages, but still limited to fixed size single messages and limited accuracy for long messages. The latter was imporved in the LogGP model which extends the LogP model with support for linear modeling of long messages~\cite{alexandrov1995loggp}. Several other refinements exist including LogGPC~\cite{moritz1998logpc} which adds parameters for network contention, LogGPS~\cite{10.1145/568014.379592} which estimates synchronisation costs in the communications library and LogGPO~\cite{chen2009loggpo} which characterrixe the overlap between communication and computation. The main drawback of LogP models are the inclusion of hard to estimate parameters such as network contention and synchronisation costs in addition to the lack of support for variable size messages and heterogenous latency and bandwidth.

% Other more recent works... our work in light of the above.

