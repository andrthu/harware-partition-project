 Data for JOB [35405,1] offset 0 Total slots allocated 4

 ========================   JOB MAP   ========================

 Data for node: n004	Num slots: 4	Max slots: 0	Num procs: 4
 	Process OMPI jobid: [35405,1] App: 0 Process rank: 0 Bound: socket 1[core 57[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../BB/../../../../../..]
 	Process OMPI jobid: [35405,1] App: 0 Process rank: 1 Bound: socket 1[core 35[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [35405,1] App: 0 Process rank: 2 Bound: socket 1[core 58[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../BB/../../../../..]
 	Process OMPI jobid: [35405,1] App: 0 Process rank: 3 Bound: socket 1[core 36[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
2 3 3
Two group cota
0 1 1
1 0 0
3 2 2
dune-size 1 7.7349e-06 7.7323e-06 7.8193e-06 7.825e-06 
dune-size 2 7.42551e-06 7.41221e-06 7.38321e-06 7.38391e-06 
dune-size 4 7.74591e-06 7.73981e-06 7.69361e-06 7.69811e-06 
dune-size 8 8.27131e-06 8.26551e-06 7.39921e-06 7.39571e-06 
dune-size 16 6.75111e-06 7.42021e-06 6.74761e-06 7.41051e-06 
dune-size 32 7.9166e-06 7.9113e-06 7.8403e-06 7.829e-06 
dune-size 64 1.0363e-05 1.03482e-05 1.03498e-05 1.03341e-05 
dune-size 128 1.52367e-05 1.52106e-05 1.54201e-05 1.5396e-05 
dune-size 256 1.75153e-05 1.74884e-05 1.73591e-05 1.73505e-05 
dune-size 512 3.03797e-05 3.03779e-05 3.06043e-05 3.06078e-05 
dune-size 1024 8.50066e-05 8.50242e-05 8.52875e-05 8.53067e-05 
dune-size 2048 0.000161464 0.000161469 0.000162506 0.000162506 
dune-size 4096 0.000313869 0.000313881 0.000312678 0.00031267 
dune-size 8192 0.000565372 0.000565375 0.000574817 0.000574806 
dune-size 16384 0.000804847 0.000804789 0.00085005 0.000850102 
dune-size 32768 0.00159821 0.0015982 0.00160132 0.00160136 
dune-size 65536 0.00324965 0.0032496 0.00319952 0.00319946 
dune-size 131072 0.00648569 0.00648195 0.00648514 0.00648205 
