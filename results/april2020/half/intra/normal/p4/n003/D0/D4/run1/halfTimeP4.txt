 Data for JOB [9723,1] offset 0 Total slots allocated 4

 ========================   JOB MAP   ========================

 Data for node: n003	Num slots: 4	Max slots: 0	Num procs: 4
 	Process OMPI jobid: [9723,1] App: 0 Process rank: 0 Bound: socket 0[core 1[hwt 0-1]]:[../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [9723,1] App: 0 Process rank: 1 Bound: socket 1[core 35[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [9723,1] App: 0 Process rank: 2 Bound: socket 1[core 36[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [9723,1] App: 0 Process rank: 3 Bound: socket 0[core 2[hwt 0-1]]:[../../BB/../../../../../../../../../../../../../../../../../../../../../../../../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
1 0 0
2 3 3
Two group cota
0 1 1
3 2 2
dune-size 1 5.57319e-06 7.35379e-06 5.59739e-06 7.37719e-06 
dune-size 2 5.28069e-06 5.27529e-06 5.29259e-06 5.28909e-06 
dune-size 4 7.86708e-06 5.60609e-06 7.79528e-06 5.53579e-06 
dune-size 8 5.75149e-06 5.73889e-06 5.78879e-06 5.77779e-06 
dune-size 16 6.06829e-06 6.07269e-06 6.11219e-06 6.11619e-06 
dune-size 32 7.14239e-06 7.13849e-06 7.18109e-06 7.18149e-06 
dune-size 64 8.90579e-06 8.91029e-06 8.99289e-06 8.99039e-06 
dune-size 128 1.24654e-05 1.24634e-05 1.2534e-05 1.25298e-05 
dune-size 256 2.00119e-05 2.00067e-05 1.96708e-05 1.96755e-05 
dune-size 512 3.25842e-05 3.25898e-05 3.29142e-05 3.29218e-05 
dune-size 1024 0.000106556 0.000106558 0.000107586 0.000107608 
dune-size 2048 0.000198862 0.000198827 0.000198933 0.000198959 
dune-size 4096 0.000379369 0.000379227 0.000383155 0.000383093 
dune-size 8192 0.000614603 0.000614573 0.000683141 0.000683166 
dune-size 16384 0.000958527 0.000958432 0.000994974 0.000995016 
dune-size 32768 0.00228038 0.00228006 0.0017134 0.00171417 
dune-size 65536 0.0034042 0.00339862 0.0034042 0.00340924 
dune-size 131072 0.0067779 0.00675866 0.00675524 0.00677584 
