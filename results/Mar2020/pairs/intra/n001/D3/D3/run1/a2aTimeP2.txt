 Data for JOB [43608,1] offset 0 Total slots allocated 2

 ========================   JOB MAP   ========================

 Data for node: n001	Num slots: 2	Max slots: 0	Num procs: 2
 	Process OMPI jobid: [43608,1] App: 0 Process rank: 0 Bound: socket 0[core 25[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../BB/../../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]
 	Process OMPI jobid: [43608,1] App: 0 Process rank: 1 Bound: socket 0[core 26[hwt 0-1]]:[../../../../../../../../../../../../../../../../../../../../../../../../../../BB/../../../../..][../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../..]

 =============================================================
dune-size 1 2.67179e-06 2.67129e-06 
dune-size 2 2.4604e-06 2.4594e-06 
dune-size 4 2.611e-06 2.6109e-06 
dune-size 8 2.8321e-06 2.8319e-06 
dune-size 16 3.22809e-06 3.22739e-06 
dune-size 32 3.98279e-06 3.98269e-06 
dune-size 64 6.24328e-06 6.24338e-06 
dune-size 128 8.69838e-06 8.69848e-06 
dune-size 256 1.49475e-05 1.4948e-05 
dune-size 512 2.75361e-05 2.75371e-05 
dune-size 1024 5.80964e-05 5.8095e-05 
dune-size 2048 0.000106467 0.000106465 
dune-size 4096 0.000212518 0.000212519 
dune-size 8192 0.00040797 0.000407969 
dune-size 16384 0.000797824 0.000797822 
dune-size 32768 0.00155617 0.00155615 
dune-size 65536 0.0030975 0.00309748 
dune-size 131072 0.00631818 0.00631759 
dune-size 262144 0.0126971 0.0126984 
dune-size 524288 0.0254739 0.025474 
dune-size 1048576 0.0508974 0.0508958 
